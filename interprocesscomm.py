# -*- coding: utf-8 -*-
"""
Client written in Python for Project III
@author: willguarienti
"""

#!/usr/bin/env python
import tkinter
import serial
import serial.tools.list_ports
import time
import socket
import os
import shutil

#Serial related info
arduino_found = False
arduino_ports = []
filePath = 'C:\\Mach3\\ArquivosPlugin\\'
filePath_git = 'C://Users//willi//Documents//ProjetoIII//projeto_iii_python'

#LOG FUNCTIONS
#Write msg on log file
def write_on_log(msg):
    global f_log
    f_log.write(time.strftime("%H:%M:%S >> ")) #write current time
    f_log.write(msg)                           #write message
    f_log.write('\n')
#Open log file
def open_log_file():
    global f_log
    global log_file_name
    log_file_name= time.strftime("Log_interprocesscomm_%Y%m%d_%H%M%S.txt")
    print('Log file ' + log_file_name + ' aberto')
    f_log = open(filePath + log_file_name, 'w')
    f_log.write(time.strftime("%H:%M:%S >> ")) #write current time
    f_log.write("Log aberto")
    f_log.write('\n')
#close log file
def close_log_file():
    global f_log
    global log_file_name
    print('Log file ' + log_file_name + ' fechado')
    f_log.write(time.strftime("%H:%M:%S >> ")) #write current time
    f_log.write("Log fechado")
    f_log.write('\n')
    f_log.close()


#START EVERYTHING!!!
open_log_file();
'''
Setup client socket
'''
#before starting the interface, connect to VS2008
HOST = '127.0.0.1'      #local host
TCP_PORT = 9999
BUFFER_SIZE = 20          # Normally 1024, but we want fast response

tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
dest = (HOST, TCP_PORT)
tcp.connect(dest)
print("Conectado ao socket")
write_on_log("Conectado ao socket")

'''
Begin communcation with Arduino
'''
def searchArduino():
    global arduino_ports
    global arduino_found
    arduino_ports = [
        p.device
        for p in serial.tools.list_ports.comports()
        if 'CH340' in p.description or 'Arduino' in p.description
    ]
    if not arduino_ports:
        lbl.configure(text="Arduino is not connected")
        arduino_found = False
        #window.after(1000,searchArduino)
    elif len(arduino_ports) > 1:
        lbl.configure(text='Multiple Arduinos found - Starting the first')
        arduino_found = True
    else:
        lbl.configure(text='Arduino detected. Starting...')
        arduino_found = True
    #------------------------------------
    if(arduino_found == True):
        global ser
        ser = serial.Serial(arduino_ports[0],9600)
        time.sleep(2)   #there's some kind of time issue with Arduino boot... we have to wait a little bit ;)
        write_on_log("Conectado ao arduino")
        window.destroy()


#tuto: https://likegeeks.com/python-gui-examples-tkinter-tutorial/

window = tkinter.Tk()
window.title("Arduino detector")
window.geometry('350x200')
lbl = tkinter.Label(window, text="Arduino wasn't detected...")
lbl.grid(column=0, row=0)

btn = tkinter.Button(window, text="Press to detect Arduino...", command=searchArduino)
btn.grid(column=1, row=0)
window.mainloop()


'''
open interface...
do what you gotta to do
'''

#TODO: read from text file
#move above the 4th axis:
msg = "G0 X10 Y20 Z0"               #<-----
tcp.send(msg.encode())
confirmation_msg = tcp.recv(BUFFER_SIZE)
confirmation_msg = confirmation_msg.decode() #why you should use decode: https://stackoverflow.com/questions/21810776/str-encode-adds-a-b-to-the-front-of-data

if(confirmation_msg != "Sucesso"):
    print("Não foi bem sucedida a movimentação para o quarto eixo")
    write_on_log("Não foi bem sucedida a movimentação para o quarto eixo")



ref = 0
n_points = 60                             #that's an user input     #<-----
step = 360/(n_points)

#Create .txt to save distance_sensor measurements...
f_distance_sensor = open(filePath + 'vl53l1x_log.txt', 'w')
f_distance_sensor.write("Numero de medidas a fazer: %d \n" %n_points)
print('Distance sensor file aberto')

#Save all measurements from vl53l1x
distance_sensor_measurements = [];

while (ref<360):

    #registra no log
    write_on_log("Nova medida %.4f" %ref)
    #Send a G1 command to mach3
    msg = "G0 A%.4f" %ref                   #<-----
    print(msg)
    tcp.send(msg.encode())
    confirmation_msg = tcp.recv(BUFFER_SIZE)   #we hope we got a sucessfull message, but there's nothing we can do if something goes wrong. Ok, I will log this problem
    confirmation_msg = confirmation_msg.decode()
    if(confirmation_msg != "Sucesso"):
        print("Não foi bem sucedida a movimentação para o quarto eixo")
        write_on_log("Não foi bem sucedida a movimentação para o quarto eixo")

    #ASK ARDUINO A MEASURE
    #After correctly positioned, begin a measurement
    ser.write(b'S')                           #ask for a new measurement
    dist_bytes = ser.readline()               #return bytes
    dist_str = dist_bytes.decode("ascii")     #convert to str
    distancia = float(dist_str)               #convert to float
    print('Medida recebida do arduino: %f' %distancia)

    #Save data from arduino
    distance_sensor_info = "[%f, %f]" %(ref,distancia)
    f_distance_sensor.write(distance_sensor_info + '\n')
    distance_sensor_measurements.append(distance_sensor_info)

    #TAKE A PICTURE
    os.chdir(filePath_git)
    os.system ('python Scripts_ProjetoII//saveFramefake.py')                    #<----
    #make a copy whose name is ref number
    std_name = filePath + 'image0.png'
    cpd_name = filePath + 'ref'+ ('%.4f'%ref) +'.png'
    cpd_name = cpd_name.replace(".","_",1)
    shutil.copy2(std_name,cpd_name)

    ref = ref + step

#after finishing measurements
msg = '#quit'
tcp.send(msg.encode())

#Imprime resultado:
print("Medidas do vl53l1x")
print(distance_sensor_measurements)

#end communication
tcp.close() #close socket
ser.close()  #close serial
f_log.close() #close log file
