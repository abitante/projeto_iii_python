################################################################################
## Python Interface for CNC 4th axis, by Guilherme Abitante and ....
################################################################################
# PROGRAMMERS, PLEASE MIND THE LANGUAGE PATTERN: English for everything, but
# inner algorithm explanation (which may be written in whatever).
################################################################################
from tkinter import *
from tkinter import ttk
import tkinter
import numpy as np
import math
from functools import partial
################################################################################
# Function is called when a data point button is clicked
# ptID : Position of data associated with button in the 
def pointClick(ptID):
    print(ptID)
    pointButtons[ptID].configure(image=pointImageExcluded)

# Function calculates the local regression error for every point
def calculateLocalRegErr():
    print("Local Linear Regression error calculi:")
    global localRegError
    localRegError = [] # Just to be sure, clearing the list
    for i in range(0, len(points)):
        localRegResult.append(localRegressionError(i,points, comparingRange))
        localRegError.append(localRegResult[i][1])
        if localRegResult[i][0][0] == None:
            print("+ Point %d with %d range: z=(%.3f) with error %.3f"% (i,comparingRange,localRegResult[i][0][1],localRegResult[i][1]))
        else:
            print("+ Point %d with %d range: z=(%.3f).y+(%.3f) with error %.3f"% (i,comparingRange,localRegResult[i][0][0],localRegResult[i][0][1],localRegResult[i][1]))
        
# Function calculates the local regression error for a point and its neighbours
def localRegressionError(pt ,pts, ptRange):
    selectedPts = []
    if ptRange*3 > len(pts): # If the end shape has less than 3 sides, error (None)
        return None
    if (pt - ptRange) < 0: # If selectedPts begins at the end of pts
        for i in range(len(pts)+(pt - ptRange), len(pts)):
            selectedPts.append(pts[i]) # get the end part first
        for i in range(0, pt+1+ptRange):
            selectedPts.append(pts[i])
    elif pt + ptRange + 1 > len(pts):
        for i in range(pt - ptRange, len(pts)):
            selectedPts.append(pts[i])
        for i in range(0, 1+pt+ptRange-len(pts)):
            selectedPts.append(pts[i])
    else: 
        for i in range(pt - ptRange, pt + ptRange + 1):
            selectedPts.append(pts[i])
    #print(selectedPts)
    return linearRegression(selectedPts)

def linearRegression(pts):# pts: [[x,z],...]
    x = []
    z = []
    error = 0
    
    for i in range(0, len(pts)):
        x.append(pts[i][0])
        z.append(pts[i][1])
    x = np.array(x)
    z = np.array(z)
    n = np.size(x)
    m_x, m_z = np.mean(x), np.mean(z)
    # Calculating cross-deviation and deviation about x
    SS_xz = np.sum(z*x) - n*m_z*m_x 
    SS_xx = np.sum(x*x) - n*m_x*m_x
    # Calculating regression coefficients (z = A.x + B)
    if SS_xx == 0: # Perfectly vertical line
        A = None
        B = m_x
        for i in range(0, len(pts)):
            error = error + abs(pts[i][0]-B)
    else: 
        A = SS_xz / SS_xx
        B = m_z - A*m_x
        if len(pts) > 2:
            for i in range(0, len(pts)):
                #simple error: error = error + pts[i][1]*pts[i][1]-(B+A*pts[i][0])*(B+A*pts[i][0])
                error = error + (abs(A*pts[i][0]+B-pts[i][1]))/(math.sqrt(A*A+1)) # possible bug in code...
        else:
            error = None
    return[[A,B],error]

# Function finds the approx. edge by interpolation of subsequent projection lines. The result is the mean of the interpolation.
# Later the code can be enhanced to detect if the edge is valid (indeed an edge) by analyzing the error.
# hc: Camera height relative to the CNC base in mm
# hp: 4th axis height from the CNC base im mm
# mea: Measures from image as [[distance in pixels of projected end of piece from center of image, angle of 4th axis when image was taken in rad],...]
# d: density of mm/pixel of measures
def geometricEdgeFinder(hc,hp,mea,d):
    print("Initializing geometricEdgeFinder")
    aOffset = mea [0][1]
    lines = []
    ptAz = []
    ptAy = []
    ptBz = []
    ptBy = []
    for i in range(0,len(mea)):
        mea [i][1] = mea [i][1] - aOffset
        ptAz.append((hc-hp)*math.cos(mea[i][1]))
        ptAy.append(-(hc-hp)*math.sin(mea[i][1]))
        ptBz.append(-hp*math.cos(mea[i][1])-d*mea[i][0]*math.sin(mea[i][1]))
        ptBy.append(hp*math.sin(mea[i][1])-d*mea[i][0]*math.cos(mea[i][1]))
        #print([[ptAy,ptAz],[ptBy,ptBz]])
        A = (ptAy[i]-ptBy[i])/(ptAz[i]-ptBz[i])
        B = ptBy[i]-A*ptBz[i]
        # y = A*z + B
        lines.append([A,B])
        #print(lines[i])
    intersection = [] # List of points of intersection
    meanY = 0 # Mean value of intesections
    meanZ = 0
    currentZ = 0
    for i in range(0,len(lines)):
        if i +1 == len(lines): # ta no final da lista
            currentZ = (lines[0][1]-lines[i][1])/(lines[i][0]-lines[0][0])
        else:
            currentZ = (lines[i+1][1]-lines[i][1])/(lines[i][0]-lines[i+1][0])
        meanY = meanY + lines[i][0]*currentZ +lines[i][1]
        meanZ = meanZ + currentZ
        intersection.append([currentZ ,lines[i][0]*currentZ +lines[i][1]])
    #print(intersection)
    meanY = meanY/len(lines)
    meanZ = meanZ/len(lines)
    error = 0 # error squared
    for i in range(0, len(lines)):
        errY = abs(abs(meanY)-abs(intersection[i][1]))*abs(abs(meanY)-abs(intersection[i][1]))
        errZ = abs(abs(meanZ)-abs(intersection[i][0]))*abs(abs(meanZ)-abs(intersection[i][0]))
        error = error + errY + errZ
    # later error can be used to identify if edge is valid or not an edge
    meanModule = math.hypot(meanY,meanZ)
    meanAngle = math.atan2(meanY,meanZ) + aOffset
    meanY = meanModule*math.sin(meanAngle)
    meanZ = meanModule*math.cos(meanAngle)
    print("+ Aprox. edge is in Y= %.3f  Z= %.3f, with error %.3f" % (meanY,meanZ, error))
    return [meanY,meanZ, error, intersection, [ptAz,ptAy,ptBz,ptBy]]

# Function returns the offset of the camera relative to the CNC coords System
# The system uses the info gathered from a known drawn line
# cnc: pos where photo was taken as [x,y,z] in mm
# ptsPix: points detected from image in pixel as [aXpix, aYpix, bXpix, bYpix]
# ptsReal = [aXmm, aYmm, bXmm, bYmm]
def calculateCameraOffset(ptsPix, ptsReal, cnc, hc):
    d1000 = 1 # Razao d quando a camera esta a 1000mm da projecao
    offset = [0,0,0,0] #[x,y,z,d], em mm no sist. coord. da CNC e onde d é a razão mm/pixel na altura hc (max da cnc)
    dh = (ptsReal[3]-ptsReal[1])/(ptsPix[3]-ptsPix[1]) # razão d na altura da cnc em q a foto foi tirada
    offset[0] = (dh*ptsPix[0])-(cnc[0]-ptsReal[0])
    offset[1] = (dh*ptsPix[1])-(cnc[1]-ptsReal[1])
    alturaRealCamera = dh*1000/d1000
    offset[2] = cnc[2] - alturaRealCamera
    offset[3] = hc*d1000/1000


#===============================================================================   
    
class secondWindow:
    def __init__(self, root, w, x, y):
        self.top = tkinter.Toplevel(root)
        self.top.geometry("%dx%d+%d+%d" % (w, w, x, y))
        
        # Creating Static Canvas, for stuff that never changes (like data points or the center
        self.canvas = tkinter.Canvas(self.top, width=w, height=w, background='white')
        self.canvas.pack(fill="both", expand=1)
        # Drawing center (+)
        self.canvas.create_line(w*0.5, w*0.46, w*0.5, w*0.54, fill="red")
        self.canvas.create_line(w*0.46, w*0.5, w*0.54, w*0.5, fill="red")

        self.scale = w # How many mm fit in a pixel (updated in populate)
        self.width = int(w) # Size in pixels of canvas
        
    # Creates Static Graphics and Buttons on Secondary Window
    # pts: list of points as [[x,z],...]
    # ptSize: side of button (int)
    def populate(self, pts, ptSize, butImage):
        distantPoint = 0        
        print('Populating points in Static Canvas...')
        # Scaling...
        for i in range(0,len(pts)):                 # Se em um ponto do conjunto
            for j in range(0,2):                    # uma das duas coordenadas deste ponto
                if abs(pts[i][j]) > distantPoint:   # for a maior em todo o conjunto
                    distantPoint = abs(pts[i][j])   # esse ponto é o absoluto mais distante
        print("+ Furthest Point: %d" % distantPoint)  # (pq a tela eh quadrada entao foda-se o modulo)
        self.scale = 0.4*secWsize/distantPoint
        print("+ Scale [pixel/mm] set as: %f" % self.scale)
        self.canvas.create_line(secWsize-20,secWsize-20,secWsize-20,secWsize-30)
        self.canvas.create_line(secWsize-20,secWsize-20,secWsize-20-self.scale,secWsize-20)
        self.canvas.create_line(secWsize-20-self.scale,secWsize-20,secWsize-20-self.scale,secWsize-30)
        ttk.Label(self.top,text="1 mm").place(x = secWsize-55,y = secWsize-19)
        # Coord System painting...
        lineLenght = 30
        coorA = [secWsize-20,secWsize-50]
        coorB = [coorA[0]-lineLenght,coorA[1]]
        coorC = [coorB[0],coorB[1]-lineLenght]
        arrowLenght= 8
        arrowWidth = 4
        self.canvas.create_line(coorA[0],coorA[1],coorA[0]-arrowLenght,coorA[1]-arrowWidth)
        self.canvas.create_line(coorA[0],coorA[1],coorB[0],coorB[1])
        self.canvas.create_line(coorA[0],coorA[1],coorA[0]-arrowLenght,coorA[1]+arrowWidth)
        self.canvas.create_line(coorC[0],coorC[1],coorC[0]-arrowWidth, coorC[1]+arrowLenght)
        self.canvas.create_line(coorC[0],coorC[1],coorB[0],coorB[1])
        self.canvas.create_line(coorC[0],coorC[1],coorC[0]+arrowWidth, coorC[1]+arrowLenght)
        ttk.Label(self.top,text="Z").place(x = coorC[0]+arrowWidth+2,y = coorC[1])
        ttk.Label(self.top,text="Y").place(x = coorA[0],y = coorA[1]-arrowWidth-12)
        
        #print('Calculated Button Positions: ')
        global pointButtons
        pointButtons = [] # to be sure...
        butPos = []
        for i in range(0,len(pts)):
            # points Z axis is inverted due to window "y" axis logic
            butPos = [int(points[i][0]*self.scale),int(-points[i][1]*self.scale)]
            #print(butPos)
            pointButtons.append(tkinter.Button(self.top, command=partial(pointClick,i), image=butImage,width="%d"%ptSize,height="%d"%ptSize, borderwidth=0, background="#FFFFFF"))
            pointButtons[i].place(x = butPos[0]+(secWsize-ptSize)/2,y = butPos[1]+(secWsize-ptSize)/2)

    # Draws a simple graphic that displays error results
    # Receives lst: [err,err2,err3,...], where negative errors represent detected nearest to edge point
    def paintLocalRegressionError(self, lst):
        maxErr = 0
        for i in range(0, len(lst)):
            if abs(lst[i]) > maxErr:
                maxErr = abs(lst[i])
        grphScale = int(0.1*self.width/maxErr)
        grphOffX = 5
        grphOffY = 5
        grphStep = int(self.width/(len(lst)+2))
        if grphStep > 15:
            grphStep = 15
        print("Making Local Error Graph with", grphOffX ,"x", grphOffY,"offset and",grphScale,"scale and",grphStep,"step")

        grphLines = []
        for i in range(0,len(lst)):
            grphLines.append(self.canvas.create_line(grphOffX+grphStep*i, self.width-grphOffY, grphOffX+grphStep*i, self.width-grphOffY-abs(lst[i])*grphScale, fill="cyan", width=3))
        return grphLines # for tracking and later deletion

#===============================================================================
def rescale(a):
    trdW.canvas.scale("all",trdW.width/2,trdW.width/2,float(a)/trdW.scale,float(a)/trdW.scale)
    trdW.scale = float(a)
        
class thirdWindow:
    def __init__(self, root, w, x, y):
        self.top = tkinter.Toplevel(root)
        self.top.geometry("%dx%d+%d+%d" % (w, w, x, y))
        
        # Creating Static Canvas, for stuff that never changes (like data points or the center
        self.canvas = tkinter.Canvas(self.top, width=w, height=w, background='white')
        self.canvas.pack(fill="both", expand=1)
        # Drawing center (+)
        self.canvas.create_line(w*0.5, w*0.1, w*0.5, w*0.9, fill="red")
        self.canvas.create_line(w*0.1, w*0.5, w*0.9, w*0.5, fill="red")
        self.width = int(w) # Size in pixels of canvas
        self.scale = 1
        Scale(self.top, from_=1, to=8, orient=HORIZONTAL, length=500, command=rescale).pack()
        
    def grphLine(self,a,b,c,d, color):
        self.canvas.create_line(self.width*0.5+a, self.width*0.5+b, self.width*0.5+c, self.width*0.5+d, fill=color)
        
    def grphThickLine(self,a,b,c,d, color,s):
        self.canvas.create_line(self.width*0.5+a, self.width*0.5+b, self.width*0.5+c, self.width*0.5+d, fill=color,width = s)
        
    def visualizeData(self,hc,hp,d,data):
        print("Generating data for third window...")
        hcp = hc - hp # distance of camera from 4th axis
        labelDistance = 20
        #   |
        #   |
        # -----> Y   /|\
        #   |        / angles
        #  \|/ Z    / 
        Ycam = [] # Y coord of camera for each measure
        Zcam = [] # Z coord of camera for each measure
        ##Mcam is always hcp # module of camera in polar system (centered in 4th axis) for each measure
        ##Acam is always data[i][1]+math.pi # angle of camera in polar system (measured counte-clockwise from Z axis) for each measure
        #Bcam = [] # internal angle of measure triangle, of the camera vertice, for each measure
        Ybase = [] # Y coord of base for each measure
        Zbase = [] # Z coord of base for each measure
        Yproj = [] # Y coord of projection for each measure
        Zproj = [] # Z coord of projection for each measure
        #Mproj = [] # module of projection in polar system (centered in 4th axis) for each measure
        #Aproj = [] # angle of projection in polar system (measured counte-clockwise from Z axis) for each measure
        #Bproj = [] # internal angle of measure triangle, of the projection vertice, for each measure
        
        for i in range(0,len(data)):
            Ycam.append(hcp*math.sin(data[i][1]+math.pi))
            Zcam.append(hcp*math.cos(data[i][1]+math.pi))
            Ybase.append(hp*math.sin(data[i][1]))
            Zbase.append(hp*math.cos(data[i][1]))
            Yproj.append(Ybase[i] + d*data[i][0]*math.sin(data[i][1]+math.pi/2))
            Zproj.append(Zbase[i] + d*data[i][0]*math.cos(data[i][1]+math.pi/2))
            
            self.grphLine(Ycam[i],Zcam[i],Ybase[i],Zbase[i],"grey") # lines from camera to base
            ttk.Label(self.top,text=("%.3f"%data[i][1])).place(x = self.width*0.5+(hcp+labelDistance)*math.sin(data[i][1]+math.pi),y = self.width*0.5+(hcp+labelDistance)*math.cos(data[i][1]+math.pi))
            self.grphLine(Ycam[i],Zcam[i],Yproj[i],Zproj[i],"blue") # lines from camera to projection
            self.grphLine(Ybase[i],Zbase[i],Yproj[i],Zproj[i],"cyan") # lines from base to projection
            '''
            Mproj.append(math.hypot(Zproj[i],Yproj[i]))
            Aproj.append(math.atan2(Zproj[i],Yproj[i]))
            L1 = math.sqrt(hcp*hcp+Mproj[i]*Mproj[i]-2*hcp*Mproj[i]*math.cos(data[i][1]+math.pi-Aproj[i])) # Distance from camera to projection
            Bcam.append(math.acos((hcp*hcp+L1*L1-Mproj[i]*Mproj[i])/(2*hcp*L1)))
            Bproj.append(- data[i][1] + Aproj[i] - Bcam[i])
            '''
            
        coef = [] # [[A,B], flag ], if (flag == 1){y = A.z + B}else{z = A.y + B}
        for i in range(0,len(data)):
            if abs(Ycam[i]-Yproj[i]) < abs(Zcam[i]-Zproj[i]) : # Using y = A.z + B equasion
                #coef.append(linearRegression([[Zcam,Ycam],[Zproj,Yproj]]))
                #coef[i][1] = 1
                A = (Ycam[i]-Yproj[i])/(Zcam[i]-Zproj[i])
                coef.append([[A,Ycam[i]-A*Zcam[i]],1])
                print("+ Measure regression %d resulted in z = %.3f.y + %.3f" % (i,coef[i][0][0],coef[i][0][1]))
            else: # Using z = A.y + B equasion
                #coef.append(linearRegression([[Ycam,Zcam],[Yproj,Zproj]]))
                #coef[i][1] = 0
                A = (Zcam[i]-Zproj[i])/(Ycam[i]-Yproj[i])
                coef.append([[A,Zcam[i]-A*Ycam[i]],0])
                print("+ Measure regression %d resulted in y = %.3f.z + %.3f" % (i,coef[i][0][0],coef[i][0][1]))
                
        intersection = []# [[y,z],...]
        for i in range(0,len(data)):
            if i  == len(data)-1:
                next_i = 0
            else:
                next_i = i + 1
                
            if coef[i][1] == 1: # y1 = A.z1 + B
                if coef[next_i][1] == 1: # y2 = A.z2 + B
                    # y = A1.z + B1 = A2.z + B2 -> z = (B1-B2)/(A2-A1)
                    z = (coef[i][0][1]-coef[next_i][0][1])/(coef[next_i][0][0]-coef[i][0][0])
                    intersection.append([coef[i][0][0]*z+coef[i][0][1],z])
                else: # z2 = A.y2 + B
                    # y = A1.z + B1 = (z - B2)/A2
                    if coef[next_i][0][0] == 0:# if A2 = 0
                        z = coef[next_i][0][1] #   z = B2
                    else:                   # else: B1.A2 + B2 = (1 - A1.A2).z
                        z = (coef[i][0][1]*coef[next_i][0][0] + coef[next_i][0][1])/(1 - coef[i][0][0]*coef[next_i][0][0])
                    intersection.append([coef[i][0][0]*z+coef[i][0][1],z])
            else: # z1 = A.y1 + B
                if coef[next_i][1] == 1: # y2 = A.z2 + B
                    # y = A2.z + B2 = (z - B1)/A1
                    if coef[i][0][0] == 0:  # if A1 = 0
                        z = coef[i][0][1]   #   z = B1
                    else:                   # else: B2.A1 + B1 = (1 - A1.A2).z
                        z = (coef[next_i][0][1]*coef[i][0][0] + coef[i][0][1])/(1 - coef[i][0][0]*coef[next_i][0][0])
                    intersection.append([coef[next_i][0][0]*z+coef[next_i][0][1],z])
                else: # z2 = A.y2 + B
                    # z = A1.y + B1 = A2.y + B2 -> y = (B1-B2)/(A2-A1)
                    z = (coef[i][0][1]-coef[next_i][0][1])/(coef[next_i][0][0]-coef[i][0][0]) # reusing var (read z as y)
                    intersection.append([z,coef[i][0][0]*z+coef[i][0][1]])

        distance = []
        edgeData = [] # list of lists that make up an edge
        edgeCounter = 0 # Position in edge list of the current edge
        maxDistance = 5
        for i in range(0, len(intersection)):            
            if i  == len(intersection)-1:
                next_i = 0
            else:
                next_i = i + 1
            d1 = intersection[i][0] - intersection[next_i][0]
            d2 = intersection[i][1] - intersection[next_i][1]
            distance.append(math.sqrt(d1*d1+d2*d2))
            if len(edgeData) <= edgeCounter: # if there are no elements in current edgeData[]
                edgeData.append([intersection[i]])
            else:
                edgeData[edgeCounter].append(intersection[i])
            if distance[i] > maxDistance: # if distance to next is great, next is a new edge
                edgeCounter = edgeCounter + 1
            print("+ Regression %d intersects the next at Y = %.3f Z = %.3f"%(i,intersection[i][0],intersection[i][1]))
            print("++ Instersection %d is part of edge %d and is %.3fmm appart from the next"%(i,len(edgeData)-1, distance[i]))

        edge = [] #
        y = 0
        z = 0
        error = 0
        for i in range(0, len(edgeData)):
            for j in range(0, len(edgeData[i])):
                y = y + edgeData[i][j][0]
                z = z + edgeData[i][j][1]
            y = y/len(edgeData[i])
            z = z/len(edgeData[i])
            for j in range(0, len(edgeData[i])):
                error = error + math.sqrt((edgeData[i][j][0]-y)*(edgeData[i][j][0]-y)+(edgeData[i][j][1]-z)*(edgeData[i][j][1]-z))
            edge.append([y,z,len(edgeData[i]),error])
            print("+ Edge %d is at Y= %.3f Z=%.3f and was calculated using %d points with %.3f error."%(i,edge[i][0],edge[i][1],edge[i][2],edge[i][3]))
            y = 0
            z = 0
            error = 0
        realEdge = []
        for i in range(0, len(edge)):
            if edge[i][2] > 1:
                realEdge.append([edge[i][0],edge[i][1]])
        for i in range(0, len(realEdge)):           
            if i  == len(realEdge)-1:
                next_i = 0
            else:
                next_i = i + 1
            self.grphThickLine(realEdge[i][0],realEdge[i][1],realEdge[next_i][0],realEdge[next_i][1],"red",3) # lines from edge to next edge
            print("+ Real Edge at Y= %.3f Z= %.3f"%(realEdge[i][0],realEdge[i][1]))
                 
'''
"Mint"² = Mcam[i]² + "L1cam[i]"² -2.Mcam[i]."L1cam[i]".cos(Bcam[i])
"Mint"² = Mproj[i]² + "L1proj[i]"² -2.Mproj[i]."L1proj[i]".cos(Bproj[i])
L1[i] = "L1cam[i]" + "L1proj[i]"

"Mint"² = Mcam[i+1]² + "L1cam[i+1]"² -2.Mcam[i+1]."L1cam[i+1]".cos(Bcam[i+1])
"Mint"² = Mproj[i+1]² + "L1proj[i+1]"² -2.Mproj[i+1]."L1proj[i+1]".cos(Bproj[i+1])
L1[i+1] = "L1cam[i+1]" + "L1proj[i+1]"

"Mint"/sin(Bproj[i]) = "L1proj[i]"/sin("Aint" - Aproj[i])
"Mint"/sin(Bproj[i+1]) = "L1proj[i+1]"/sin("Aint" - Aproj[i+1])
"Mint"/sin(Bcam[i]) = "L1cam[i]"/sin(Acam[i] - "Aint")
"Mint"/sin(Bcam[i+1]) = "L1cam[i+1]"/sin(Acam[i+1] - "Aint")
'''
            
            
        
        
#===============================================================================
# Interface functions
def chkLocRegErr():
    global localRegErrorLines
    if chkVLocRegErr.get() == 1:
        if localRegError == []:
            calculateLocalRegErr()
        localRegErrorLines = secW.paintLocalRegressionError(localRegError)
    else:
        for i in range(0,len(localRegErrorLines)):
            secW.canvas.delete(localRegErrorLines[i])

################################################################################
################################################################################
## PROGRAM DESCRIPTION
        # Behaviour description:
        # - Upon clicking on a point it toggles between forcing/unforcing nearest of edge
        # - Upon clicking on an edge, it becomes selected
        # - The Menu has two sliders: # of edges and precision
        # - The Menu has three final options: "OK", "Edit" and "Cancel"
        # - The Menu displays data form the calculated shape:
        #   - Overall error with shape
        #   - if cylinder then
        #       display radius
        #     XZ of selected edge

        # Update Function description:
        # 00. Check if cylinder
        # 01. Search for "# of edges" points nearest to edges (forced and calculated)
        # 02. Conditioned linear regression of each set of points
        # 03. Error calculation for each set of points
        # 04. Real edge location approximation for the shape
        # 05. Secondary Window painting update
        # 06. Secondary Window widget positioning

    # High-level Algorithm description:
    # B00. Receive data list: [[photos],[distances in mm],[angles in rad], [cnc position in which photos were taken]]
    # B01. Obtain position for camera and 4th axis
    # B02. Command CNC to referentiate tool
    # B03. Extract poitive and negative measure from photos and store in list
    # B04. Calculate camera-based section
    # B05. Validate camera-based section based on user info (like the piece number of faces)
    # B06. Calculate sensor-based section
    # B07. Join calculated sections, respecting the informed number of faces
    # C01. Create windows and info widgets
    # C02. Populate canvas with points and camera-based section
    # C03. Calculate error of measures and display it
    # C04. Calculate edges position based on user precision.
    # C05. Populate canvas with true section and true edges
    # C06. Wait for user input
        # IF change in precision, recalculate the position value that is set when an edge-button is clicked
        # IF EDIT is clicked, popup pops with current selected edge, open for edit, with options close and OK
            # IF OK, current selected edge is used to zero the CNC
        # IF OK, current selected edge is used to zero the CNC
        # IF CANCEL, close and everything is discarded

# Step A00 of algorithm: Create Window
rootW = Tk() 
rootW.title("GREWEN")
# Determining Secondary Window Size
ws = rootW.winfo_screenwidth()  # Width  of the screen
hs = rootW.winfo_screenheight() # Height of the screen
if ws > hs :
    ws = hs
else :
    hs = ws
secWsize = 0.8*ws # Secondary window size is 80% of the smaller screen size
print("Calculated Secondary Window Size: %d" % secWsize)
# Positioning of Both Windows
secW = secondWindow(rootW, secWsize, 50, 50)
trdW = thirdWindow(rootW, secWsize, 50, 50)
rootW.geometry('%dx%d+%d+%d' % (250, 500, 100 + secWsize, 50))
#===============================================================================
# Place for Variables
localRegErrorLines = []
localRegError = []
localRegResult = []
comparingRange = 2

pointButtons = []

overallErr = StringVar()
selectedEdge = StringVar()
#===============================================================================
# Step A01 of algorithm: Main Window Widget Creation
ttk.Label(rootW,text="Number of Faces").pack()
scale_sections = Scale(rootW, from_=3, to=12, orient=HORIZONTAL, length=200)
scale_sections.set(4)
scale_sections.pack()

Label(rootW, text = "------------------------").pack()
ttk.Label(rootW,text="Positioning Resolution (mm)").pack()
scale_snap = Scale(rootW, from_=0, to=5, resolution=0.01, orient=HORIZONTAL, length=200)
scale_snap.set(0.1)
scale_snap.pack()

overallErr.set("not initialized")
selectedEdge.set("X= %f  Z= %f" % (0, 0))

Label(rootW, text = "------------------------").pack()
Label(rootW, text =  "Overall Error").pack()
Label(rootW, textvariable = overallErr).pack()
Label(rootW, text = "------------------------").pack()
Label(rootW, text =  "Selected Edge").pack()
Label(rootW, textvariable = selectedEdge).pack()
Label(rootW, text = "------------------------").pack()

chkVLocRegErr = IntVar()
Checkbutton(rootW, text="Local Regression Error",variable=chkVLocRegErr, command=chkLocRegErr).pack()

bbH = "1"
bbW = "10"
Label(rootW, text = "------------------------").pack()
Button(rootW, text="OK",width=bbW,height=bbH).pack()
Button(rootW, text="Edit",width=bbW,height=bbH).pack()
Button(rootW, text="Cancel",width=bbW,height=bbH).pack()#, command=partial(pointClick,i)
#===============================================================================
# Step A02 of algorithm: Receiving and pre-processing data
# Data Gathering:
# ReceivedData = [[modulo,angulo]...]
# Function to convert receiver data from polar to catesian form
##def polarToCartesian(ptsP):
##    print("Converting data from polar to cartesian system...")
##    # Maybe scale the angle here? (bc its in rads)
##    ptsC = []
##    for i in range(0,len(ptsP)):
##        ptsC.append([ptsP[i][0]*math.cos(ptsP[i][1]),ptsP[i][0]*math.sen(ptsP[i][1]]])
##    return ptsC
# while sockets arent ready, points are entered manually
points = [[9,11],[5,10],[0,10],[-5,10],
          [-10,10],[-10,5],[-10,0],[-10,-5],
          [-10,-10],[-5,-10],[0,-10],[5,-10],
          [10,-10],[10,-5],[10,0],[10,5]]#x,y
#===============================================================================
# Step A03 of algorithm: Data Processing
calculateLocalRegErr()
#===============================================================================
# Step A04 of algorithm: Data Display
pointImageNormal = PhotoImage(file="bpt.gif")
pointImageExcluded = PhotoImage(file="rpt.gif")
pointImageForced = PhotoImage(file="pt.gif")
secW.populate(points, 10, pointImageNormal)

# Example for the geometricEdgeFinder, which uses data from camera to estipulate a bounding shape to the workpiece
'''
hc = 225
hp = 40
d = 40/264 # mm/pix
data = [[289,math.radians(15)],
        [307,math.radians(30)],
        [300,math.radians(45)],
        [275,math.radians(60)],
        [304,math.radians(75)],
        [304,math.radians(90)],
        [287,math.radians(105)],
        [256,math.radians(120)],
        [201,math.radians(135)],
        [152,math.radians(150)],
        [183,math.radians(165)],
        [243,math.radians(180)],
        [221,math.radians(195)],
        [258,math.radians(210)],
        [279,math.radians(225)],
        [277,math.radians(240)],
        [258,math.radians(255)],
        [269,math.radians(270)],
        [283,math.radians(285)],
        [279,math.radians(300)],
        [250,math.radians(315)],
        [204,math.radians(330)],
        [139,math.radians(345)],
        [194,math.radians(360)]]
'''
hc = 254
hp = 40
d = 50/279 # mm/pix
data = [[198,math.radians(15)],
        [225,math.radians(30)],
        [239,math.radians(45)],
        [238,math.radians(60)],
        [219,math.radians(75)],
        [225,math.radians(90)],
        [236,math.radians(105)],
        [234,math.radians(120)],
        [215,math.radians(135)],
        [176,math.radians(150)],
        [121,math.radians(165)],
        [159,math.radians(180)],
        [201,math.radians(195)],
        [234,math.radians(210)],
        [253,math.radians(225)],
        [255,math.radians(240)],
        [238,math.radians(255)],
        [245,math.radians(270)],
        [259,math.radians(285)],
        [251,math.radians(300)],
        [225,math.radians(315)],
        [180,math.radians(330)],
        [119,math.radians(345)],
        [160,math.radians(360)]]
trdW.visualizeData(hc,hp,d,data)

mainloop()
