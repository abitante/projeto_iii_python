#serial related things
import serial
import time

ser = serial.Serial('COM4',9600)
time.sleep(1)

stringtoSend = "Init"

while (stringtoSend != "Q\n"):
    stringtoSend = input("Digite um comando (""Q"" para encerrar):\n")
    stringtoSend = stringtoSend + "\n"
    ser.write(stringtoSend.encode('utf-8'))
    dist_bytes = ser.readline()
    print("Received:")
    dist_str = dist_bytes.decode("ascii")
    print(dist_str)
    #distancia = float(dist_str)               #convert to float
    #print('msg recv from arduino: %f' %distancia)

ser.close()  #close serial
