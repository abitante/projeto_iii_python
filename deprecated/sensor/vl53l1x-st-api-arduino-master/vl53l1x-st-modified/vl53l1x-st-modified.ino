

/*
                 --------------------------------  VERY IMPORTANT ----------------------
                 The VIN pin should be connected to a 2.6 V to 5.5 V source, and GND should be
                 connected to 0 volts. An on-board linear voltage regulator converts VIN to a 2.8 V
                 supply for the VL53L1X IC. Note that if your input voltage is under 3.5 V, you can
                 connect it directly to VDD instead to bypass the regulator; in this configuration,
                 VIN should remain disconnected.
*/
#include <Wire.h>
#include "vl53l1_api.h"

VL53L1_Dev_t                   dev;
VL53L1_DEV                     Dev = &dev;

VL53L1_UserRoi_t roiConfig;
VL53L1_CalibrationData_t configData;


String command = "";
String StringtoSend = "";

int status;
boolean newData = false;

void setup()
{
  uint8_t byteData;
  uint16_t wordData;

  Wire.begin();
  Wire.setClock(400000);
  Serial.begin(9600);

  Dev->I2cDevAddr = 0x52;

  VL53L1_software_reset(Dev);

  /*
      The VL53L1_WaitDeviceBooted() function ensures that the device is booted and ready. This function is optional. It is a blocking function because there is an internal
      polling. This function should not be blocking for more than 4 ms, assuming 400 kHz I2C and 2 ms latency per transaction.
  */
  status = VL53L1_WaitDeviceBooted(Dev);

  /*
      The VL53L1_DataInit() function is called once. It performs device initialization. It is called once and only once after the device is brought out of reset.
      InitThe VL53L1_StaticInit() function allows loading of the device settings that are specific for a given use case.
  */
  status = VL53L1_DataInit(Dev);
  status = VL53L1_StaticInit(Dev);

  /*
     Distance mode is a parameter provided to optimize the internal settings and tunings to get the best ranging performances depending on the ranging distance required
     by the application and the ambient light conditions.

                                        Short  --> Up to 1.3 (Better ambient immunity)
                                        Medium --> Up to 3 m
                                        Long   --> Up to 4 m (Maximum distance)

  */
  status = VL53L1_SetDistanceMode(Dev, VL53L1_DISTANCEMODE_SHORT);

  /*
     Timing budget is the time required by the sensor to perform one range measurement. The minimum and maximum timing budgets are [20 ms, 1000 ms]

     The inter-measurement period is the delay between two ranging operations. An inter-measurement period can be programmed. When a ranging completes, the device waits
     for the end of the programmed inter-measurement period before resuming the next ranging. In the inter-measurement period, the sensor is in a low-power state.

  */
  status = VL53L1_SetMeasurementTimingBudgetMicroSeconds(Dev, 1000000);
  status = VL53L1_SetInterMeasurementPeriodMilliSeconds(Dev, 1000); // reduced to 50 ms from 500 ms in ST example

  /*
     Due to assembly tolerances, the optical center of the device can vary. The optical center of the device is measured for each part.
     The optical center coordinates are stored in the device NVM. The host can use these two coordinates to better align the ROI to the optical center.

     VL53L1_GetCalibrationData(Dev, &configData);
     Serial.print("Coordenada centro SPADs (x,y): ");
     Serial.print(configData.optical_centre.x_centre >> 4);  //4.4 format
     Serial.print(" ,");
     Serial.println(configData.optical_centre.y_centre >> 4);  //4.4 format
     x_centre = y_centre = 7
  */



  /*
     The receiving SPAD array of the sensor includes 16x16 SPADs which cover the full field of view (FoV). It is possible to program a smaller region of interest (ROI),
     with a smaller number of SPADs, to reduce the FoV. To set a ROI different than the default 16x16 one, the user can call the VL53L1_SetUserROI() function.
     The ROI is a square or rectangle defined by two corners: top left and bottom right.
  */
  roiConfig.TopLeftX  = 6;
  roiConfig.TopLeftY  = 9;
  roiConfig.BotRightX = 9;
  roiConfig.BotRightY = 6;
  status = VL53L1_SetUserROI(Dev, &roiConfig);

  /*
      The VL53L1_StartMeasurement() function must be called to start a measurement.
  */
  status = VL53L1_StartMeasurement(Dev);

  if (status)
  {
    Serial.println(F("VL53L1_StartMeasurement failed"));
    while (1);
  }

}

void loop()
{
  char endMarker = '\n';
  char rc;

  while (Serial.available() > 0 && newData == false)
  {
    rc = Serial.read();
    if (rc != endMarker)
       command.concat(char(rc));
    else
      newData = true;
  }

  if(newData == true)
  {
    if (command == "S") {
      static VL53L1_RangingMeasurementData_t RangingData;
  
      /*
      To get consistent results, it is mandatory to call this function after getting the ranging measurement. If this function is not called, the next ranging will start
      and the results will be updated. But, the data ready status flag will not be updated, and the physical interrupt pin will not be cleared.
      */
      VL53L1_ClearInterruptAndStartMeasurement(Dev);
  
      /*
      The function VL53L1_WaitMeasurementDataReady() polls on the device interrupt status until ranging data are ready.This function blocks all other operations
      on the host as long as the function is not completed, because an internal polling is performed.
      */
      status = VL53L1_WaitMeasurementDataReady(Dev);
      if (!status)
      {
        /*
        The Vl53L1_GetRangingMeasurementData()can be used to get ranging data. When calling this function to get the device ranging results, a structure called
        VL53L1_RangingMeasurementData_t is returned. The VL53L1_RangingMeasurementData_t structure is composed of:
  
        • TimeStamp: not implemented, please ignore it.
        • StreamCount: this 8-bit integer counter increments at each range. The value starts at 0, increments to 255, and then increments from 128 to 255.
        • RangingQualityLevel: not implemented, please ignore it.
        • SignalRateRtnMegaCps: this value is the return signal rate in MegaCountPer Second (MCPS). It is a 16.16 fix point value.To obtain a real value it should be divided by 65536.
        • AmbientRateRtnMegaCps: this value is the return ambient rate (in MCPS). It is a 16.16 fix point value, which is effectively a measure of the infrared light.
        To obtain a real value it should be divided by 65536.
        • EffectiveSpadRtnCount: this 16 bit integer returns the effective SPAD count for the current ranging. To obtain a real value it should be divided by 256.
        • SigmaMilliMeter: this 16.16 fix point value is an estimation of the standard deviation of the current ranging, expressed in millimeters.
        To obtain a real value it should be divided by 65536.
        • RangeMilliMeter: this 16 bit integer give the range distance in millimeters.
        • RangeFractionalPart: not implemented, please ignore it.
        • RangeStatus: this 8 bit integer gives the range status for the current measurement. A value of 0 means the ranging is valid
  
        0      VL53L1_RANGESTATUS_RANGE_VALID        Ranging measurement is valid
        1      VL53L1_RANGESTATUS_SIGMA_FAIL         Raised if sigma estimator check is above the internal defined threshold
        2      VL53L1_RANGESTATUS_SIGNAL_FAIL        Raised if signal value is below the internal defined threshold
        4      VL53L1_RANGESTATUS_OUTOFBOUNDS_FAIL   Raised when phase is out of bounds
        5      VL53L1_RANGESTATUS_HARDWARE_FAIL      Raised in case of HW or VCSEL failure
        7      VL53L1_RANGESTATUS_WRAP_TARGET_FAIL   Wrapped target, not matching phases
        8      VL53L1_RANGESTATUS_PROCESSING_FAIL    Internal algorithm underflow or overflow
        14     VL53L1_RANGESTATUS_RANGE_INVALID      The reported range is invalid
  
        */
        status = VL53L1_GetRangingMeasurementData(Dev, &RangingData);
      }
  
      if (!status)
      {
        StringtoSend = RangingData.RangeMilliMeter;
      }
      else
      {
        StringtoSend = "-" + status;     //In case of an invalid measurement, the negative value of the error code is sent
      }
      Serial.println(StringtoSend);
    }
    else if (command == "Q\n")          // NÃO SEI SE É NECESSÁRIO UM COMANDO "QUIT". PODEMOS TIRAR ESSA PARTE, CASO ASSIM SEJA DECIDIDO
    {
      Serial.println("-100");          // -100 is sent when the "Quit" command is received
      while (1)                        // When the command Q\n is received, arduino will enter in a while 1 and it won't answer new commands
      {}
    }
    else
      Serial.println("-10");          // -10 is sent when a incorrect command is received
  
    command = "";
    newData = false;
  }
}
