#serial related things
import serial
import time
import numpy as np
import matplotlib.pyplot as plt

ser = serial.Serial('COM7',9600)
time.sleep(2)

stringtoSend = []
distanceVector = []     #sensor_data
cartesian = [] #cartesian coordinates
current_angle = 0 

def convert(point):
    phi = -point[0]     #TODO: check this
    rho = point[1]
    z = rho * np.cos(np.radians(phi))
    y = rho * np.sin(np.radians(phi))
    return(y, z)

def pol2cart():
    for point in distanceVector:
        print("opa")
        cartesian.append(convert(point))

def plot_cartesian(cartesian_data): 
    x_arr = []
    y_arr = []
    for x,y in cartesian_data:
        x_arr.append(x)
        y_arr.append(y)
        
    plt.figure()
    plt.scatter(x_arr,y_arr,s = 20)
    ax = plt.gca()
    ax.set_ylim([-300, 300]) #z data
    ax.set_xlim([-300, 300]) #x data
    plt.gca().invert_xaxis()
    
    ax.spines['left'].set_position('center')
    ax.spines['right'].set_color('none')
    ax.spines['bottom'].set_position('center')
    ax.spines['top'].set_color('none')
    #ax.spines['left'].set_smart_bounds(True)
    #ax.spines['bottom'].set_smart_bounds(True)
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')


while (stringtoSend != "Q\n" and current_angle!=-1):
        
    dumb_user = True
    while(dumb_user):
        try:        #proof me wrong
            current_angle = input("Insira o ângulo atual:")
            current_angle = float(current_angle)
            dumb_user = False
        except:
            print("do you know what a number is?")
            dumb_user = True
                    
    if current_angle==-1 :
        pass
    else:

        distancia = 0
        for i in range(0,3):
            stringtoSend = "S\n"
            ser.write(stringtoSend.encode('utf-8'))
            dist_bytes = ser.readline()
            dist_str = dist_bytes.decode("ascii")
            print("Received:", dist_str)   
            try:
                distancia = distancia + float(dist_str)
                
            except:
                print("Error converting to float. Result>> distance = -1")
                distancia = -1;

        distancia = distancia / 3.0
            
        try:
            if distancia < 1 or distancia > 500:
                raise ValueError('Distance out of bounds')
            distanceVector.append([current_angle, distancia])
        except ValueError as e:
            print("tratando a exceção:" + str(e))
            

ser.close()  #close serial

print(distanceVector)
pol2cart()
plot_cartesian(cartesian)

