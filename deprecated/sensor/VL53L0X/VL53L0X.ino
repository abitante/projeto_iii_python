//                 ------------------------------------------ SENSOR LIBRARY, OBJECTS AND STRUCTS ------------------------------------------

#include "Adafruit_VL53L0X.h"

Adafruit_VL53L0X ToF_Sensor = Adafruit_VL53L0X();
VL53L0X_RangingMeasurementData_t measure;
VL53L0X_Error Status;

String command = "";
String StringtoSend = "";

boolean newData = false;

void setup() {
  Serial.begin(9600);

  //                  ------------------------------------------ SENSOR SETUP ------------------------------------------

  if (!ToF_Sensor.begin(VL53L0X_I2C_ADDR, false, &Wire)) {
    Serial.println(F("Failed to boot VL53L0X"));
    while (10);
  }
  //Serial.println("");
  //Serial.println("");

  //Serial.println("Distance(mm), Angle");

}


void loop() {
  char endMarker = '\n';
  char rc;

  while (Serial.available() > 0 && newData == false)
  {
    rc = Serial.read();
    if (rc != endMarker)
      command.concat(char(rc));
    else
      newData = true;
  }

  if (newData == true)
  {
    if (command == "S") {
      ToF_Sensor.rangingTest(&measure, false); // pass in 'true' to get debug data printout!
      StringtoSend = measure.RangeMilliMeter;

      Serial.println(StringtoSend);
    }
    else if (command == "Q\n")          // NÃO SEI SE É NECESSÁRIO UM COMANDO "QUIT". PODEMOS TIRAR ESSA PARTE, CASO ASSIM SEJA DECIDIDO
    {
      Serial.println("-100");          // -100 is sent when the "Quit" command is received
      while (1)                        // When the command Q\n is received, arduino will enter in a while 1 and it won't answer new commands
      {}
    }
    else
      Serial.println("-10");          // -10 is sent when a incorrect command is received

    command = "";
    newData = false;
  }
}


//  -------------------------------------------  Data available on VL53L0X_RangingMeasurementData_t struct  -----------------------------------------------------

//Serial.print("MeasurementTimeUsec:");  Serial.println(measure.MeasurementTimeUsec,HEX);   /*!< Give the Measurement time needed by the device to do the measurement.*/
//Serial.print("TimeStamp:");  Serial.println(measure.TimeStamp,HEX);   /*!< 32-bit time stamp. */
//Serial.print("Distance (mm): "); Serial.println(measure.RangeMilliMeter);  /*!< range distance in millimeter. */
//Serial.print("RangeDMaxMilliMeter:");  Serial.println(measure.RangeDMaxMilliMeter);     /*!< Tells what is the maximum detection distance of the device in current setup and environment conditions (Filled when applicable) */
//Serial.print("SignalRateRtnMegaCps:");  Serial.println(measure.SignalRateRtnMegaCps);    /*!< Return signal rate (MCPS)\n these is a 16.16 fix point value, which is effectively a measure of target reflectance.*/
//Serial.print("AmbientRateRtnMegaCps:");  Serial.println(measure.AmbientRateRtnMegaCps);    /*!< Return ambient rate (MCPS)\n these is a 16.16 fix point value, which is effectively a measure of the ambien t light.*/
//Serial.print("EffectiveSpadRtnCount:");  Serial.println(measure.EffectiveSpadRtnCount);    /*!< Return the effective SPAD count for the return signal. To obtain Real value it should be divided by 256 */
//Serial.print("ZoneId:");  Serial.println(measure.ZoneId);                                     /*!< Denotes which zone and range scheduler stage the range data relates to. */
//Serial.print("RangeFractionalPart:");  Serial.println(measure.RangeFractionalPart);    /*!< Fractional part of range distance. Final value is a FixPoint168 value. */
//Serial.print("RangeStatus:");  Serial.println(measure.RangeStatus);    /*!< Range Status for the current measurement. This is device dependent. Value = 0 means value is valid. See \ref RangeStatusPage */
