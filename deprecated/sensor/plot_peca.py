import numpy as np
import matplotlib.pyplot as plt
import pylab
cartesian = []

def convert(point):
    phi = point[0]     #TODO: check this
    rho = point[1]
    z = rho * np.cos(np.radians(phi))
    y = rho * np.sin(np.radians(phi))
    return(y, z)

def pol2cart():
    for point in distanceVector:
        cartesian.append(convert(point))

def plot_cartesian(cartesian_data): 
    x_arr = []
    y_arr = []
    for x,y in cartesian_data:
        x_arr.append(x)
        y_arr.append(y)
        
    plt.figure()
    plt.scatter(x_arr,y_arr,s = 20)
    ax = plt.gca()
    ax.set_ylim([-300, 300]) #z data
    ax.set_xlim([-300, 300]) #x data
    plt.gca().invert_xaxis()

    ax.spines['left'].set_position(('data', 0.0))
    ax.spines['bottom'].set_position(('data', 0.0))
    ax.spines['right'].set_color('none')
    ax.spines['top'].set_color('none')
    #ax.spines['left'].set_smart_bounds(True)
    #ax.spines['bottom'].set_smart_bounds(True)
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')


dvec = [[0.0, 160.66666666666666], [12.0, 166.66666666666666], [24.0, 167.66666666666666], [36.0, 170.0], [48.0, 155.66666666666666], [60.0, 163.66666666666666],
 [72.0, 158.0], [84.0, 161.0], [96.0, 164.66666666666666], [108.0, 152.66666666666666],
 [120.0, 173.33333333333334], [132.0, 174.33333333333334], [144.0, 172.66666666666666], [156.0, 171.33333333333334], [168.0, 149.66666666666666],
 [180.0, 157.33333333333334], [192.0, 167.0], [204.0, 170.33333333333334], [216.0, 172.0], [228.0, 152.66666666666666], [240.0, 160.0], [252.0, 155.0],
 [264.0, 159.66666666666666], [276.0, 163.66666666666666], [288.0, 155.0], [300.0, 173.33333333333334], [312.0, 172.66666666666666], [324.0, 171.33333333333334],
 [336.0, 169.66666666666666], [348.0, 158.0]]

for i in range (0,len(dvec)):
    [ang,dist] = dvec[i]
    dvec[i] = [ang, (178 - dist)]

distanceVector = dvec
pol2cart()
plot_cartesian(cartesian)
pylab.show()
