# -*- coding: utf-8 -*-
"""
Created on Mon Jun 10 22:07:44 2019
Simplet TCP SERVER
@author: willi
"""

import socket
HOST = ''              # Endereco IP do Servidor
PORT = 9997            # Porta que o Servidor esta

tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
orig = (HOST, PORT)
tcp.bind(orig)
tcp.listen(1)
while True:
    con, cliente = tcp.accept()
    while True:
        msg = con.recv(20)
        str_received = msg.decode()
        print(str_received)
        con.send(msg)    
        if not msg: break
    con.close()
