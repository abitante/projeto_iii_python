import cv2
import time
import shutil
from tkinter import *

filePath = 'C:\\Mach3\\ArquivosPlugin\\'
#filePath = 'C:\\Users\\Emerson\\Desktop\\'

class Erro_Window:
    
    def __init__(self, master, error):
        self.master = master
        self.master.title("ERRO")
        self.tryAgain_flag = False
        if error == "NO_CAMERA":
            self.label = Label(self.master, text="Não foi possível realizar a captura\n Verifique se a câmera está conectada")
        elif error == "BLACK_IMAGE":
            self.label = Label(self.master, text="Foi capturada uma imagem completamente preta.\n Verifique se a câmera não está encoberta")
        self.label.pack()
        self.cancel = Button(self.master, text="Cancel", command=self.master.destroy)
        self.cancel.pack(side=LEFT)
        self.tryAgain_but = Button(self.master, text="Tentar Novamente", command=self.tryAgain_met)
        self.tryAgain_but.pack(side=RIGHT)
        
    def tryAgain_met(self):
        self.master.destroy()
        self.tryAgain_flag = True
        
def window(error):
    root = Tk()
    root.geometry("350x100+550+300")
    window1 = Erro_Window(root, error)
    root.mainloop()
    return window1.tryAgain_flag
        

trying = True
blackImage = True

while(trying):
    cam = cv2.VideoCapture(1+cv2.CAP_DSHOW)
    if cam is not None and cam.isOpened():
        #cam.release()
        #cam = cv2.VideoCapture(cv2.CAP_DSHOW)
        cam.set(cv2.CAP_PROP_FRAME_WIDTH,1280)
        cam.set(cv2.CAP_PROP_FRAME_HEIGHT,960)
        for i in range(0,2):
            time.sleep(1)                          #sleep() para garatir inicializacao        
            retval, frame = cam.read()

        if retval==True:
            for i in range(0, frame.shape[0]-1):
                for j in range(0, frame.shape[1]-1):
                    R,G,B=frame[i][j]
                    if R > 50 or G > 50 or B > 50:
                        blackImage=False
                        break
                if not blackImage:
                    cv2.imwrite(filePath + 'image0.png', frame)
                    std_name = filePath + 'image0.png'
                    cpd_name = filePath + 'backup\\' + time.strftime("image_%H_%M_%S.png")
                    shutil.copy2(std_name,cpd_name)
                    cam.release()
                    trying = False
                    break
                
            if blackImage:
                trying=window("BLACK_IMAGE")   
                

        else:
            if cam.isOpened():
                cam.release()
            trying=window("NO_CAMERA")

    else:
        if cam.isOpened():
            cam.release()
        trying = window("NO_CAMERA")


            
