# -*- coding: utf-8 -*-
"""
Created on Sun Apr 28 15:14:23 2019

@author: willi
"""

import time
filePath = 'C:\\Mach3\\ArquivosPlugin\\'

#Write msg on log file
def write_on_log(msg):
    global f_log
    f_log.write(time.strftime("%H:%M:%S >> ")) #write current time
    f_log.write(msg)                        #write message
    f_log.write('\n')
#Open log file
def open_log_file():
    global f_log
    global log_file_name 
    log_file_name= time.strftime("Log_python1_%Y%m%d_%H%M%S.txt")
    print('Log file ' + log_file_name + ' aberto')
    f_log = open(filePath + log_file_name, 'w')

#close log file
def close_log_file():
    global f_log
    global log_file_name 
    print('Log file ' + log_file_name + ' fechado')
    f_log.close()
    
    
open_log_file()
write_on_log('oi' + 'sou' + 'will')
write_on_log('tchau')
close_log_file()