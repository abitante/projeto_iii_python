# -*- coding: utf-8 -*-
"""
Created on Sun Apr 28 15:46:38 2019

@author: willi
"""

import glob
import os
import cv2

list_of_files = glob.glob('C:\Mach3\ArquivosPlugin\*.jpg') # * means all if need specific format then *.csv
latest_file = max(list_of_files, key=os.path.getctime)
print(latest_file)
img = cv2.imread(latest_file) #a imagem que saveframe salva eh sempre image0.png
cv2.namedWindow('image', cv2.WINDOW_NORMAL)
cv2.imshow('image',img)
cv2.waitKey(0)
cv2.destroyAllWindows()