import numpy as np
import cv2
from scipy import signal
from matplotlib import pyplot as plt

def rotate_bound(image, angle):
    # grab the dimensions of the image and then determine the
    # center
    (h, w) = image.shape[:2]
    (cX, cY) = (w // 2, h // 2)
 
    # grab the rotation matrix (applying the negative of the
    # angle to rotate clockwise), then grab the sine and cosine
    # (i.e., the rotation components of the matrix)
    M = cv2.getRotationMatrix2D((cX, cY), -angle, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])
 
    # compute the new bounding dimensions of the image
    nW = int((h * sin) + (w * cos))
    nH = int((h * cos) + (w * sin))
 
    # adjust the rotation matrix to take into account translation
    M[0, 2] += (nW / 2) - cX
    M[1, 2] += (nH / 2) - cY
 
    # perform the actual rotation and return the image
    return cv2.warpAffine(image, M, (nW, nH))

def findEdges(img, debug = False, threshhold = 15, minsizesegment = 10):
    """
    rotacionar a imagem pra que se as faces fiquem verticais
    usar imagem RGB, acho que se por preto e branco buga porque só tem 1 valor por pixel, em vez de 3
    debug = True mostra a imagem, a "média" dela achatada verticalmente e segmentos que ele
    encontrou e plots mostrando onde foi encontrado 
    diferenças grandes de um pixel para o outro; diferença definida pelo threshhold
    threshhold: quanto maior, mais edges vai detectar
    minsizesegment: o tamanho mínimo entre duas faces; a detecçAõ é baseada em picos de diferença de pixels,
    mas como pode haver dois picos seguidos, é necessário ignorar o segundo
    retorna as duas edges mais próximas do centro
    """
    
    avg = img[0]*0
    for row in img:
        avg=avg+row
    avg = avg/len(img)
    
    #FIltragem
    b, a = signal.butter(1, 0.25)
    ar = []
    ag = []
    ab = []
    for index, element in enumerate(avg):
        ar.append(element[0])
        ag.append(element[1])
        ab.append(element[2])
    ar = np.array(ar)
    ag = np.array(ag)
    ab = np.array(ab)
    arf = signal.filtfilt(b, a, ar, padlen=100)
    agf = signal.filtfilt(b, a, ag, padlen=100)
    abf = signal.filtfilt(b, a, ab, padlen=100)

    #diferença entre cada 4 pixels
    diffr = np.diff(arf[range(0,len(arf),4)],axis=0)
    diffg = np.diff(agf[range(0,len(agf),4)],axis=0)
    diffb = np.diff(abf[range(0,len(abf),4)],axis=0)
    
    #acha se alguma cor tem um pico maior que o threshhold
    peaks = []
    peaksIndexes = []
    for index,element in enumerate(diffr):
        if index>15:
            if np.sqrt(element**2) > threshhold:
                peaks.append(element)
                peaksIndexes.append(index*4 + 4)
            elif np.sqrt(diffg[index]**2) > threshhold:
                peaks.append(element)
                peaksIndexes.append(index*4 + 4)
            elif np.sqrt(diffb[index]**2) > threshhold:
                peaks.append(element)
                peaksIndexes.append(index*4 + 4)
            
    #organiza as faces em segmentos (para o projeto não serve pra nada porque a gente só pega as duas bordas mais próximas do centro)
    indexSegments = [[0]]
    insideSegment = True
    for index, element in enumerate(peaks):
        if not insideSegment:
            insideSegment = True
    #        indexSegments[-1].append(peaksIndexes[index])
        elif insideSegment and (peaksIndexes[index] - indexSegments[-1][0]) > minsizesegment:
            
            insideSegment = False
            indexSegments[-1].append(peaksIndexes[index])
            indexSegments.append([peaksIndexes[index]])
            
    if len(indexSegments[-1])==1:
        indexSegments[-1].append(len(ar)-1)
    #acha as duas bordas mais próximas do centro
    middleSegment = [0,0]
    end = False
    for element in indexSegments:
        for index in element:
            middleSegment[0] = middleSegment[1]
            middleSegment[1] = index
            if middleSegment[0] <= len(arf)/2 and middleSegment[1] >= len(arf)/2:
                end =True
        if end:
            break
    
             
    if debug:
        newImage = avg.copy()*0
        for index,element in enumerate(ar):
            for indexSeg ,segment in enumerate(indexSegments):
                if index in segment:
                    newImage[index-1:index+1] = np.array([255,255,255])
               # if index >= segment[0] and index < segment[1]:
                    #newImage.append(np.mod(indexSeg,2)*255)
       # newImage = np.array(newImage)   
        newImage = np.tile(newImage,(100,1,1))  
        avg = np.tile(avg,(100,1,1)) 
        plt.figure()
        plt.plot(arf)
        plt.plot(ar)
        plt.plot(agf)
        plt.plot(ag)
        plt.plot(abf)
        plt.plot(ab)
        
        plt.plot(range(4,len(arf),4),diffr)
        plt.plot(range(4,len(arf),4),diffg)
        plt.plot(range(4,len(arf),4),diffb)
        plt.plot(peaksIndexes,peaks,'*')
        
        edges = cv2.Canny(avg.astype('uint8'),30,15)
        cv2.imshow('image',newImage.astype('uint8'))
        cv2.imshow('image2',avg.astype('uint8'))
        cv2.imshow('image3',img.astype('uint8'))
        k = cv2.waitKey(0)
        cv2.destroyAllWindows()
    return middleSegment

#EXEMPLOS
img = cv2.imread('imagem5.jpg').astype('float')
img = rotate_bound(img, 90)
answer = findEdges(img,threshhold=13,debug=True)
print(answer)
img = cv2.imread('Untitled.png').astype('float')
answer = findEdges(img,threshhold=13,debug=True)
print(answer)
img = cv2.imread('railroad.png').astype('float')
answer = findEdges(img,threshhold=13,debug=True)
print(answer)
img = cv2.imread('imagem4.jpg').astype('float')
img = rotate_bound(img, 90)
answer = findEdges(img,threshhold=13,debug=True)
print(answer)
img = cv2.imread('madeira.png').astype('float')
img = rotate_bound(img, 90)
answer = findEdges(img,threshhold=25,debug=True)
print(answer)