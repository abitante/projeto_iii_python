# -*- coding: utf-8 -*-
"""
@authors: Guilherme Raabe Abitante e William Cechin Guarienti
"""
#tem q testar limites de coordenada e caso ja esteja no limite, dz que o aparato ta mto na ponta

from scipy.spatial import distance as dist
from imutils import perspective
from imutils import contours
import numpy as np
import argparse
import imutils
import cv2
import math
import subprocess
import time #for logging purposes
filePath = 'C:\\Mach3\\ArquivosPlugin\\'

#Write msg on log file
def write_on_log(msg):
    global f_log
    f_log.write(time.strftime("%H:%M:%S >> ")) #write current time
    f_log.write(msg)                           #write message
    f_log.write('\n')
#Open log file
def open_log_file():
    global f_log
    global log_file_name
    log_file_name= time.strftime("Log_python1_%Y%m%d_%H%M%S.txt")
    print('Log file ' + log_file_name + ' aberto')
    f_log = open(filePath + log_file_name, 'w')
    f_log.write(time.strftime("%H:%M:%S >> ")) #write current time
    f_log.write("Log aberto")
    f_log.write('\n')
#close log file
def close_log_file():
    global f_log
    global log_file_name
    print('Log file ' + log_file_name + ' fechado')
    f_log.write(time.strftime("%H:%M:%S >> ")) #write current time
    f_log.write("Log fechado")
    f_log.write('\n')
    f_log.close()

#function related to point border check
def pointBorder(point, rectX = 1280,rectY = 960, thresh = 50):
    if (point[0] < thresh)|(point[0] > rectX - thresh)|(point[1] < thresh)|(point[1] > rectY - thresh):
            return(True)
    return(False)

fs = open(filePath + 'setup.txt', 'r')
setupText = fs.readlines()
fs.close()

refXYZ = [int(s) for s in setupText[6].split() if s.isdigit()]
rData = [int(s) for s in setupText[7].split() if s.isdigit()]
dimencaoAparato = [int(s) for s in setupText[8].split() if s.isdigit()]
offsetCamera1000 =  [int(s) for s in setupText[9].split() if s.isdigit()]
areaMinimaDeteccao =  [int(s) for s in setupText[10].split() if s.isdigit()]
limitesXY =  [int(s) for s in setupText[11].split() if s.isdigit()]
cameraToSpindle =  [int(s) for s in setupText[12].split() if s.isdigit()]

fc = open(filePath + 'control.txt', 'r')
controlText = fc.readlines()
currentCNCpos = [int(s) for s in controlText[1].split() if s.isdigit()]
fc.close()

#Calcula a razão r [mm/pixel]
r = (refXYZ[2]*rData[0])/(rData[1]*rData[2])
print('\n------\nRazao mm/pixel :'+str(r))

#Open log:
open_log_file()


#  ___    _                ___  _    _               _       ___ _____ ___  ___
# | __|__| |_ ___   ___   / _ \| |__| |_ ___ _ _  __| |___  | _ \_   _/ _ \/ __|
# | _/ _ \  _/ _ \ / -_) | (_) | '_ \  _/ -_) ' \/ _` / _ \ |  _/ | || (_) \__ \
# |_|\___/\__\___/ \___|  \___/|_.__/\__\___|_||_\__,_\___/ |_|   |_| \___/|___/


##TODO: melhorar a confiabilidade dessa parte.
#######>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#Tirando a foto.
subprocess.call(filePath + 'saveFrame\\saveFrame.exe')

#Obtendo soh o vermelho da foto.
img = cv2.imread(filePath + 'image0.png') #a imagem que saveframe salva eh sempre image0.png

red_filtered = cv2.GaussianBlur(img, (9,9), 0)
hsv = cv2.cvtColor(red_filtered, cv2.COLOR_BGR2HSV)
mask1 = cv2.inRange(hsv, (0, 100, 100), (7, 255, 255))
mask2 = cv2.inRange(hsv, (175, 100, 100), (180, 255, 255))
mask = cv2.bitwise_or(mask1, mask2)
only_red = cv2.bitwise_and(red_filtered,red_filtered, mask=mask)

#Retirando imperfeicoes da foto.
edged = cv2.Canny(only_red, 50, 150)
edged = cv2.dilate(edged, None, iterations=1)
edged = cv2.erode(edged, None, iterations=1)
#cv2.imshow("Image",cv2.resize(edged, (1280, 624)))

#Identifica os contornos
cnts = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE) #acha os pts dos contornos:
#https://docs.opencv.org/3.3.0/d9/d8b/tutorial_py_contours_hierarchy.html
#https://docs.opencv.org/3.0.0/d4/d73/tutorial_py_contours_begin.html
#cnts = cnts[0] if imutils.is_cv2() else cnts[1]
cnts = cnts[1] if imutils.is_cv3() else cnts[0] #add compatibility with OpenCv 4.0

if len(cnts) >0:
    (cnts, _) = contours.sort_contours(cnts)    #ordena os contornos da esqr. pra direita, de baixo pra cima

    boxes = []
    for c in cnts:
        box = cv2.minAreaRect(c) #retorna o pt do centro, a largura e a altura, e o ângulo # [[centro x, centro y],[largura X, largura Y],angulo]
        areaBox = box[1][0]*box[1][1]
        if areaBox < areaMinimaDeteccao[0]:   #soh pega os contornos grandes
            continue

        box = cv2.cv.BoxPoints(box) if imutils.is_cv2() else cv2.boxPoints(box)    #pega os pts do menor retangulo
        box = np.array(box)
        box = perspective.order_points(box)
        boxes.append([areaBox,box]) #adiciona na lista de boxes

    '''
    HIPÓTESE:
    Antes da execução desse código:
        Ou não se obteve nenhum sinal de vida do aparato
        Ou o aparato foi PARCIALMENTE identificado, porque estava na borda.

    CONCLUSÃO:
        O maior retângulo vermelho encontrado na foto é o aparato.
    '''

    #Ordena os retângulos conforme o seu tamanho, posicionando o maior na última posição.
    if len(boxes) > 0:
        for iter_num in range(len(boxes)-1,0,-1):
            for idx in range(iter_num):
                if boxes[idx][0]>boxes[idx+1][0]:
                    temp = boxes[idx]
                    boxes[idx] = boxes[idx+1]
                    boxes[idx+1] = temp

        #Armazena em box os 4 pontos do maior retângulo e em areaBox a respectiva área
        areaBox = boxes[len(boxes)-1][0]
        box = boxes[len(boxes)-1][1]
        print('-----\nArea do objeto encontrado: ' + str(areaBox))
        print('Objeto encontrado:')
        print(box)
        write_on_log('-----\nArea do objeto encontrado: ' + str(areaBox))
        
    else: boxes = None
else: boxes = None

'''
Decide se:
    I. Vai para o próximo ponto do setup.txt, caso nenhum sinal de vida do aparato fora detectado. A próxima etapa continua sendo 3 (python2.exe deve ser executado novamente).
    II. Calcula um novo ponto, uma vez que o aparato foi parcialmente detectado. A próxima etapa continua sendo 3 (python2.exe deve ser executado novamente).
    III. O aparato está completamente englobado pela foto. Calcula ponto para centralizar o aparato. A próxima etapa é a 4 (python3.exe deve ser executado)
'''

#CASO I: Aparato não foi encontrado
if boxes is None:
    print("\n\n------\nAparato não encontrado.")
    write_on_log("Caso1: Aparato não encontrado.")
    nextCNCpos = None
    for count in range(0,5):
        #Acha a posição corrente para determinar a próxima
        CNCpositions = [int(s) for s in setupText[count].split() if s.isdigit()]
        if (CNCpositions[0]==currentCNCpos[0]) & (CNCpositions[1]==currentCNCpos[1]):       #Se quiser comparar para Z...
            nextCNCpos = setupText[count+1]     #A posição corrente é setupText[count], a próxima é setupText[count+1]
            print('Reposicionando a CNC para:')
            print(nextCNCpos)
            #Escreve no arquivo a próxima posição
            fc = open(filePath + 'control.txt', 'w')
            fc.write('1\n' + nextCNCpos)
            fc.close()
            write_on_log('Reposicionando a CNC para: ' + nextCNCpos)
            write_on_log('Texto do control.txt: '+ '1 '+ nextCNCpos)
            close_log_file()
            break
    if nextCNCpos is None:
        print('O aparato não está na mesa.')
        fc = open(filePath + 'control.txt', 'w')
        fc.write('5\n0 0 0')
        fc.close()
        write_on_log('O aparato não está na mesa.')
        write_on_log('Texto do control.txt: 5 0 0 0')
        close_log_file()

elif(pointBorder(box[0]))|(pointBorder(box[1]))|(pointBorder(box[2]))|(pointBorder(box[3])):    #CASO II: Aparato parcialmente encontrado
    Xmid = (box[0][1] + box[2][1])/2
    Ymid = (box[0][0] + box[2][0])/2

    print("\n\n--------\nAparato na borda, com as coordenadas centrais (com a distância paralela ao eixo X da CNC primeiro):")
    print(str(Xmid) + ' ' + str(Ymid))# Expliquei o q eh o q no print, pra ter ctz
    print('A foto foi tirada com a CNC em:')
    print(str(currentCNCpos[0]) + ' ' + str(currentCNCpos[1]))

    write_on_log("Caso2: Aparato parcialmente encontrado")
    write_on_log("Aparato na borda, com as coordenadas centrais (com a distância paralela ao eixo X da CNC primeiro): " + str(Xmid) + ' ' + str(Ymid))
    write_on_log('A foto foi tirada com a CNC em:' + str(currentCNCpos[0]) + ' ' + str(currentCNCpos[1]))

    #Estimando uma posição de modo que o aparato fique aproximadamente no centro da próxima foto
    nextX =  int(currentCNCpos[0] + (960/2 - Xmid)*r)
    nextY =  int(currentCNCpos[1] + (1280/2 - Ymid)*r)
    if nextX > limitesXY[0]: nextX = limitesXY[0] #pra evitar que a mesa va ao infinito
    if nextY > limitesXY[1]: nextX = limitesXY[1]
    nextCNCpos = [nextX*(nextX>0), nextY*(nextY>0), 0]      #se for menor do que zero, faz igual a 0

    if (currentCNCpos[0]==limitesXY[0])|(currentCNCpos[1]==limitesXY[1]):
        print('Máquina CNC não pode ser reposicionada, pois atingiu um limite máximo. Reposicione o aparato e reinicie o processo.')
        #Escreve no arquivo a próxima posição
        f = open(filePath + 'control.txt', 'w')
        f.write('6\n' + str(nextCNCpos[0]) + ' ' + str(nextCNCpos[1]) + ' ' + str(nextCNCpos[2]))
        f.close()
        
        write_on_log('Máquina CNC não pode ser reposicionada, pois atingiu um limite máximo. Reposicione o aparato e reinicie o processo.')
        write_on_log('Texto do control.txt: ' + '6 ' + str(nextCNCpos[0]) + ' ' + str(nextCNCpos[1]) + ' ' + str(nextCNCpos[2]))
        close_log_file()
        
    else:
        print('Reposicionando a CNC em:' + str(nextCNCpos[0]) + ' ' + str(nextCNCpos[1]) + ' ' + str(nextCNCpos[2]))
        f = open(filePath + 'control.txt', 'w')
        f.write('1\n' + str(nextCNCpos[0]) + ' ' + str(nextCNCpos[1]) + ' ' + str(nextCNCpos[2]))
        f.close()
        
        write_on_log('Reposicionando a CNC em:' + str(nextCNCpos[0]) + ' ' + str(nextCNCpos[1]) + ' ' + str(nextCNCpos[2]) )
        write_on_log('Texto do control.txt: ' + '1 ' + str(nextCNCpos[0]) + ' ' + str(nextCNCpos[1]) + ' ' + str(nextCNCpos[2]))
        close_log_file()

else:
    print("\n\n------\nAparato completamente encontrado!")
    write_on_log("Caso3: Aparato completamente encontrado")
    #       _ _                          _   _               _           _                                _
    #  __ _| | |_ _  _ _ _ __ _   ___ __| |_(_)_ __  __ _ __| |__ _   __| |___   __ _ _ __  __ _ _ _ __ _| |_ ___
    # / _` | |  _| || | '_/ _` | / -_|_-<  _| | '  \/ _` / _` / _` | / _` / _ \ / _` | '_ \/ _` | '_/ _` |  _/ _ \
    # \__,_|_|\__|\_,_|_| \__,_| \___/__/\__|_|_|_|_\__,_\__,_\__,_| \__,_\___/ \__,_| .__/\__,_|_| \__,_|\__\___/
    #                                                                                |_|

    areaReal = dimencaoAparato[0]*dimencaoAparato[1]
    alturaEstimada = math.sqrt(areaReal) * refXYZ[2] / (math.sqrt(areaBox) * r)
    print('Altura estimada (mm): ' + str(alturaEstimada))
    write_on_log('Altura estimada (mm): ' + str(alturaEstimada))

    #       __     _   _               __ _    _      _      _                                _
    # __ __/_/ _ _| |_(_)__ ___   ___ / _(_)__(_)__ _| |  __| |___   __ _ _ __  __ _ _ _ __ _| |_ ___
    # \ V / -_) '_|  _| / _/ -_) / _ \  _| / _| / _` | | / _` / _ \ / _` | '_ \/ _` | '_/ _` |  _/ _ \
    #  \_/\___|_|  \__|_\__\___| \___/_| |_\__|_\__,_|_| \__,_\___/ \__,_| .__/\__,_|_| \__,_|\__\___/
    #                                                                    |_|

    '''
    Nesse instante, deseja-se descobrir qual é o vértice do aparato que não é cercado de vermelho.
    '''
    lista = [[],[],[],[]]
    l = 10      #metade do lado do quadrado de pesquisa de vermelho

    #Calcula a presença de pixels vermelhos ao redor dos vértices do aparato
    for i in range(0,4):
        lista[i] = [np.sum ( mask [ int(box[i][1]-l): int(box[i][1]+l+1) , int(box[i][0]-l):int(box[i][0]+l+1) ] ) , i]#soma os pixels vermelhos num quadrado de lado 2*l centrado no vértice identificado

    #print(lista)
    #Bubble sort para ordenar os pontos conforme a presença de pixels vermelhos
    for iter_num in range(len(lista)-1,0,-1):
        for idx in range(iter_num):
            if lista[idx][0]>lista[idx+1][0]:
                temp = lista[idx]
                lista[idx] = lista[idx+1]
                lista[idx+1] = temp
    #print(lista)

    oposto_index = lista[0][1]
    oposto_x = box[oposto_index][0]
    oposto_y = box[oposto_index][1]

    '''
    Na sequência, é identificado o vértice do zero-peça, que é o mais distante do ponto que não é cercado de vermelho (oposto_x, oposto_y)
    '''

    lista =[[],[],[]]
    j = 0
    #Calcula as distâncias em relação ao vértice oposto
    for i in range(0,4):
        if i!=oposto_index:
            lista[j] = [dist.euclidean((box[i][0], box[i][1]), (oposto_x, oposto_y)), i]
            j = j + 1

    #print(lista)
    #Bubble sort para ordenar as distâncias em relação aos vértices opostos (lista esta em ordem crescente de distancia ao ponto oposto)
    for iter_num in range(len(lista)-1,0,-1):
        for idx in range(iter_num):
            if lista[idx][0]>lista[idx+1][0]:
                temp = lista[idx]
                lista[idx] = lista[idx+1]
                lista[idx+1] = temp

    #print(lista)

    #O vértice mais distante é o zero-peça. Assim:
    zeropc_index = lista[2][1]
    zeropc_x = box[zeropc_index][0]
    zeropc_y = box[zeropc_index][1]

    #  ___ _           _ _             /\/|
    # | __(_)_ _  __ _| (_)_____ _ __ |/\/ ___
    # | _|| | ' \/ _` | | |_ / _` / _/ _` / _ \
    # |_| |_|_||_\__,_|_|_/__\__,_\__\__,_\___/
    #                              )_)

    # Distância em pixels do vértice oficial ao centro
    vertice_ao_centro_projetado_x = 640 - zeropc_x
    vertice_ao_centro_projetado_y = 480 - zeropc_y
    vertice_ao_centro_x = vertice_ao_centro_projetado_x * alturaEstimada / refXYZ[2]
    vertice_ao_centro_y = vertice_ao_centro_projetado_y * alturaEstimada / refXYZ[2]

    print('Distância horizontal da origem da foto até o vértice do aparato (pixel): ' + str(zeropc_x))
    print('Distância  vertical  da origem da foto até o vértice do aparato (pixel): ' + str(zeropc_y))
    print('Distância horizontal do centro da foto até o vértice do aparato (pixel): ' + str(vertice_ao_centro_x))
    print('Distância  vertical  do centro da foto até o vértice do aparato (pixel): ' + str(vertice_ao_centro_y))
    
    write_on_log('Distância horizontal da origem da foto até o vértice do aparato (pixel): ' + str(zeropc_x))
    write_on_log('Distância  vertical  da origem da foto até o vértice do aparato (pixel): ' + str(zeropc_y))

    # Qual a posição da câmera no sist. ca CNC?
    posCameraX = currentCNCpos[0] +  offsetCamera1000[0] - 1000
    posCameraY = currentCNCpos[1] +  offsetCamera1000[1] - 1000

    # Quais as coordenadas XYZ reais do aparato no sist. da CNC?

    Xreal = posCameraX + vertice_ao_centro_y * r
    Yreal = posCameraY + vertice_ao_centro_x * r
    print('X'+str(Xreal) + ' Y' + str(Yreal))
    Zreal = alturaEstimada
    write_on_log('Coordenadas XYZ reais do aparato no sist. da CNC:')
    write_on_log('X:'+str(Xreal) + ' Y:' + str(Yreal)+ ' Z:' + str(Zreal))

    # Qual o ângulo do aparato?
    ey_index = lista[0][1]
    ey_x = box[ey_index][0]
    ey_y = box[ey_index][1]
    ex_index = lista[1][1]
    ex_x = box[ex_index][0]
    ex_y = box[ex_index][1]

    '''
    Calcula o ângulo do sistema de coordenadas do aparato
    '''
    ang_ap_1 = math.degrees(math.atan2((zeropc_x - ex_x), (zeropc_y - ex_y)))
    ang_ap_2 = math.degrees(math.atan2( (ey_y-zeropc_y), (zeropc_x - ey_x)))
    #calcula a média
    ang_ap = 0.5 * (ang_ap_1 + ang_ap_2)
    if ang_ap < 0:
        ang_ap = 360 + ang_ap
    print('\n\nAngulo Calculado:')
    print(ang_ap)
    write_on_log('Angulo Calculado:' + str(ang_ap))

    h = math.sqrt(dimencaoAparato[0]*dimencaoAparato[0]+dimencaoAparato[1]*dimencaoAparato[1])
    beta = math.radians(ang_ap)+ math.atan2(dimencaoAparato[1],dimencaoAparato[0])
    dx = h* math.cos(beta)* 2/3
    dy = h* math.sin(beta)* 2/3
    flag = 1
    if int(Xreal + dx) < 0:
        if abs(abs(dx) - abs(Xreal))>10:
            flag = 0
        dx = - Xreal

    if int(Yreal + dy) < 0:
        if abs(abs(dy) - abs(Yreal))>10:
            flag = 0
        dy = - Yreal


    # Escrevendo resultados em constants.txt
    f = open(filePath + 'constants.txt', 'a')
    f.write(str(int(Xreal + dx)) + ' ' + str(int(Yreal + dy)) + ' '+ str(int(alturaEstimada-cameraToSpindle[0])) + '\n')     #step4; posição do "centro" do aparato
    f.write(str(alturaEstimada) + '\n')                                                               # Altura estimada do aparato no sist. da CNC
    f.write(str(Xreal) + ' ' + str(Yreal) + ' ' + str(Zreal) + '\n')                                  # Pos real do vértice do aparato no sist. da CNC
    f.write(str(ang_ap) + '\n')                                                                       # Angulo do aparato em torno de Z

    if (Xreal > 0) & (Yreal > 0) & (alturaEstimada > 150) & (alturaEstimada < 400) & flag:
        f.write('1\n')
        write_on_log('Centro do Aparato é atingível e está na mesa de trabalho')
    else:                                                   #se deu certo ou não
        f.write('0\n')
        write_on_log('Centro do Aparato NÃO é atingível ou NÃO está na mesa de trabalho')
    f.close()

# Editando para substituir pontos por virgulas
    s = open(filePath + 'constants.txt').read()
    s = s.replace('.', ',')
    f = open(filePath + 'constants.txt', 'w')
    f.write(s)
    f.close()

# Passa pra próxima usando control.txt
    fc = open(filePath + 'control.txt', 'w')
    fc.write('2\n'+ str(int(Xreal + dx)) + ' ' + str(int(Yreal + dy)) + ' '+ str(int(alturaEstimada-cameraToSpindle[0])))
    fc.close()
    
#Fecha o log    
    write_on_log('Texto do control.txt: ' + '2 ' + str(int(Xreal + dx)) + ' ' + str(int(Yreal + dy)) + ' '+ str(int(alturaEstimada-cameraToSpindle[0])))
    close_log_file()