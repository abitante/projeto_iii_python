# -*- coding: utf-8 -*-
"""
Creates a custom logger for Project III
@author: willi
"""

import logging

def log(module_name,file_name):
    # Create a custom logger
    logger = logging.getLogger(module_name)
    logger.setLevel(logging.DEBUG)  #https://stackoverflow.com/questions/17668633/what-is-the-point-of-setlevel-in-a-python-logging-handler
    if (not(logger.hasHandlers())):
        # Create handlers
        c_handler = logging.StreamHandler()
        f_handler = logging.FileHandler('C:\\Mach3\\ArquivosPlugin\\'+file_name+'.log')          #('file.log')
        c_handler.setLevel(logging.DEBUG)
        f_handler.setLevel(logging.INFO)

        # Create formatters and add it to handlers
        c_format = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
        f_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        c_handler.setFormatter(c_format)
        f_handler.setFormatter(f_format)

        # Add handlers to the logger
        logger.addHandler(c_handler)
        logger.addHandler(f_handler)

    return logger
