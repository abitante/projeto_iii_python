"""
@author: willi
convert data acquired from distance sensor: polar->cartesian
phi: angle
pho: lenght
"""

import numpy as np
import matplotlib.pyplot as plt

def convert(point):
    phi = -point[0]     #TODO: check this
    rho = point[1]
    z = rho * np.cos(np.radians(phi))
    y = rho * np.sin(np.radians(phi))
    return(y, z)

def pol2cart(distance_sensor_measurements):
    contents = []        #from vl53l1x log file
    polar = []           #polar coordinates
    cartesian = []
    #sensor_data
    
    contents = [x.strip("[ ] \n ") for x in distance_sensor_measurements]#read contents and remove black spaces
    for line in contents:
        [phi, pho] = line.split(",") 
        polar.append([float(phi),float(pho)])
    
    for point in polar:
        cartesian.append(convert(point))
    
    return cartesian

def plot_cartesian(cartesian_data): 
    x_arr = []
    y_arr = []
    for x,y in cartesian_data:
        x_arr.append(x)
        y_arr.append(y)
        
    plt.figure()
    plt.scatter(x_arr,y_arr,s = 20)
    ax = plt.gca()
    ax.set_ylim([-100, 100]) #z data
    ax.set_xlim([-100, 100]) #x data
    plt.gca().invert_xaxis()
    
    ax.spines['left'].set_position('center')
    ax.spines['right'].set_color('none')
    ax.spines['bottom'].set_position('center')
    ax.spines['top'].set_color('none')
    #ax.spines['left'].set_smart_bounds(True)
    #ax.spines['bottom'].set_smart_bounds(True)
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
