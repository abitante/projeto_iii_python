# -*- coding: utf-8 -*-
"""
Created on Sat Jun 15 21:39:44 2019
Code that opens the pictures taken and gets the relevant info 
@author: will and ernest
https://www.troyfawkes.com/learn-python-multithreading-queues-basics/
"""
import mylog
import numpy as np
import cv2
from matplotlib import pyplot as plt
from scipy import signal
from scipy.signal import find_peaks



class crop_params:
    #where you clicked the mouse
    mouseX = 0
    mouseY = 0
    
    #crop parameters for the workpiece
    WPy_crop1 = 300
    WPy_crop2 = 460
    WPx_crop1 = 320
    WPx_crop2 = 1200
    
    #crop parameters for mandril
    My_crop1 = 580
    My_crop2 = 650
    Mx_crop1 = 400
    Mx_crop2 = 1200


def get_mouse(event,x,y,flags,param):     #used to identify where you clicked the mouse
    global mouseX,mouseY
    if event == cv2.EVENT_LBUTTONDBLCLK:
        mouseX,mouseY = x,y
        
def reject_outliers(data):
    m = 2
    u = np.mean(data)
    s = np.std(data)
    filtered = [e for e in data if (u - m* s <= e <= u + m*s)]
    if not filtered:         #empty list. it's not supposed to happen
        return u
    
    return filtered

def rotate_bound(image, angle):
    # grab the dimensions of the image and then determine the
    # center
    (h, w) = image.shape[:2]
    (cX, cY) = (w // 2, h // 2)

    # grab the rotation matrix (applying the negative of the
    # angle to rotate clockwise), then grab the sine and cosine
    # (i.e., the rotation components of the matrix)
    M = cv2.getRotationMatrix2D((cX, cY), -angle, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])

    # compute the new bounding dimensions of the image
    nW = int((h * sin) + (w * cos))
    nH = int((h * cos) + (w * sin))

    # adjust the rotation matrix to take into account translation
    M[0, 2] += (nW / 2) - cX
    M[1, 2] += (nH / 2) - cY

    # perform the actual rotation and return the image
    return cv2.warpAffine(image, M, (nW, nH))

def findEdges(img, debug = False, threshholdblue = 15, threshholdred = 15, threshholdgreen = 15, minsizesegment = 10):
    """
    rotacionar a imagem pra que se as faces fiquem verticais
    usar imagem RGB, acho que se por preto e branco buga porque só tem 1 valor por pixel, em vez de 3
    debug = True mostra a imagem, a "média" dela achatada verticalmente e segmentos que ele
    encontrou e plots mostrando onde foi encontrado
    diferenças grandes de um pixel para o outro; diferença definida pelo threshhold
    threshhold: quanto maior, mais edges vai detectar
    minsizesegment: o tamanho mínimo entre duas faces; a detecçAõ é baseada em picos de diferença de pixels,
    mas como pode haver dois picos seguidos, é necessário ignorar o segundo
    retorna as duas edges mais próximas do centro
    """

    avg = img[0]*0 #create zero array
    for row in img:
        avg=avg+row
    avg = avg/len(img)

    #FIltragem
    b, a = signal.butter(1, 0.25)
    ar = []
    ag = []
    ab = []
    for index, element in enumerate(avg):
        ar.append(element[0])
        ag.append(element[1])
        ab.append(element[2])
    ar = np.array(ar)
    ag = np.array(ag)
    ab = np.array(ab)
    arf = signal.filtfilt(b, a, ar, padlen=100)
    agf = signal.filtfilt(b, a, ag, padlen=100)
    abf = signal.filtfilt(b, a, ab, padlen=100)

    step_pixels = 1
    #diferença entre cada step_pixels pixels
    diffr = np.diff(arf[range(0,len(arf),step_pixels)],axis=0)
    diffg = np.diff(agf[range(0,len(agf),step_pixels)],axis=0)
    diffb = np.diff(abf[range(0,len(abf),step_pixels)],axis=0)

    #acha se alguma cor tem um pico maior que o threshhold
    peaks = []
    peaksIndexes = []
    for index,element in enumerate(diffr):
        if index>15:
            if np.sqrt(element**2) > threshholdblue: #TODO: are you sure?
                peaks.append(element)
                peaksIndexes.append(index*step_pixels + step_pixels)
            elif np.sqrt(diffg[index]**2) > threshholdgreen: #TODO: are you sure?
                peaks.append(diffg[index])
                peaksIndexes.append(index*step_pixels + step_pixels)
            elif np.sqrt(diffb[index]**2) > threshholdred:
                peaks.append(diffb[index])
                peaksIndexes.append(index*step_pixels + step_pixels)

    #find local maximums. if local maximums are too far from the edge, use the edge!
    x, properties = find_peaks([abs(number) for number in peaks], prominence=0, distance = 1)
    x_peaks = []
    y_peaks = []
    for i in x:
        x_peaks.append(peaksIndexes[i])
        y_peaks.append(peaks[i])
    
    max_dist2first = 8
    max_dist2last = 8

    if(abs(x_peaks[0]-peaksIndexes[0])>max_dist2first):
        x_peaks[0] = peaksIndexes[0]
        y_peaks[0] = peaks[0]
        
    if(abs(x_peaks[-1]-peaksIndexes[-1])>max_dist2last):
        x_peaks[-1] = peaksIndexes[-1]
        y_peaks[-1] = peaks[-1]
    
    old_peaks = peaks 
    old_peaksIndexes = peaksIndexes
    peaks = y_peaks
    peaksIndexes = x_peaks 
    
    #organiza as faces em segmentos (para o projeto não serve pra nada porque a gente só pega as duas bordas mais próximas do centro)
    #first element
    indexSegments = [[0]]
    for index, element in enumerate(peaksIndexes):
        if (peaksIndexes[index] - indexSegments[-1][0]) > minsizesegment:
            indexSegments[-1].append(peaksIndexes[index])
            indexSegments.append([peaksIndexes[index]])

    #last element
    if len(indexSegments[-1])==1:
        indexSegments[-1].append(len(ar)-1)

    if debug:
        newImage = avg.copy()*0
        for index,element in enumerate(ar):
            for indexSeg ,segment in enumerate(indexSegments):
                if index in segment:
                    newImage[index-1:index+1] = np.array([255,255,255])
               # if index >= segment[0] and index < segment[1]:
                    #newImage.append(np.mod(indexSeg,2)*255)
       # newImage = np.array(newImage)
        newImage = np.tile(newImage,(100,1,1))
        avg = np.tile(avg,(100,1,1))
        plt.figure()
        plt.plot(peaksIndexes,peaks,'or')
        plt.plot(old_peaksIndexes,old_peaks,'*k')

    #   plt.figure()
    #   plt.plot(arf)
    #   plt.plot(ar)
    #   plt.plot(agf)
    #   plt.plot(ag)
    #   plt.plot(abf)
    #   plt.plot(ab)   #<------- RED

    #   plt.plot(range(step_pixels,len(arf),step_pixels),diffr)
    #   plt.plot(range(step_pixels,len(arf),step_pixels),diffg)
    #   plt.plot(range(step_pixels,len(arf),step_pixels),diffb)
      # edges = cv2.Canny(avg.astype('uint8'),30,15)
      # cv2.imshow('image',newImage.astype('uint8'))
      # cv2.imshow('image2',avg.astype('uint8'))
      # cv2.imshow('image3',img.astype('uint8'))
      # k = cv2.waitKey(0)
      # cv2.destroyAllWindows()
    return indexSegments


def slice_and_findEdges(crop_img, num_slices = 10, debug = False):
#slice picture in 10 different parts
    segments_set = []
    lb_vector = []
    rb_vector = []
    current_slice = 0
    num_rows = len(crop_img)

    while current_slice < num_slices:
        im_slice = crop_img[int(current_slice*((1/num_slices)*num_rows)):int((current_slice+1)*((1/num_slices)*num_rows)),:]
        current_slice = current_slice + 1
        segments_set.append(findEdges(im_slice, False,  threshholdblue=5, threshholdgreen=5, threshholdred=4, minsizesegment=5))
        
    #get the candidates for borders
    for element in segments_set:
        if(len(element)<=2):
            continue
        try:
            lb_vector.append(element[0][1])
        except IndexError:
            pass
        try:
            rb_vector.append(element[-1][0])
        except IndexError:
            pass


    lb = int(round(np.mean(reject_outliers(lb_vector))))
    rb = int(round(np.mean(reject_outliers(rb_vector))))
    
    clean_lb_vector = reject_outliers(lb_vector)
    clean_rb_vector = reject_outliers(rb_vector)
    angles = []
    for index,element in enumerate(clean_lb_vector):
        angles.append(np.arctan2(len(crop_img)/num_slices*(index + 0.5),clean_lb_vector[0] - element))
    for index,element in enumerate(clean_rb_vector):
        angles.append(np.arctan2(len(crop_img)/num_slices*(index + 0.5),clean_rb_vector[0] - element))
    print(np.rad2deg(np.mean(angles)))
    angle = np.rad2deg(np.mean(angles))
    
    if debug:
        print('lb_vector:',reject_outliers(lb_vector))
        print('lb: ',lb)
        print('rb_vector:',reject_outliers(rb_vector))
        print('rb: ',rb)
        newImage = np.zeros(crop_img.shape[1:]) #black image
        newImage[lb-1:lb+1] = np.array([0,0,255])
        newImage[rb-1:rb+1] = np.array([0,0,255])
        newImage = np.tile(newImage,(len(crop_img),1,1))
        rotatedCropImg = rotate_bound(crop_img, angle - 90).astype('uint8')
        for j,element in enumerate(rotatedCropImg):
            for i, element2 in enumerate(rotatedCropImg[0]):
#                print(str(i) + " " + str(j))
                if i == lb or i == rb:
                    rotatedCropImg[j,i] = [0,0,255]
#        rotatedCropImg[:,lb-1:lb+1] = np.tile(np.array([0,0,255]),(len(crop_img),1,1))
#        rotatedCropImg[:,rb-1:rb+1] = np.tile(np.array([0,0,255]),(len(crop_img),1,1))

#        rotate_bound(image, angle)
        crop_img = crop_img.astype('uint8')
        rotatednewImage = rotate_bound(newImage, -angle + 90).astype('uint8')
        cv2.imshow('original_cropped',crop_img)
        cv2.imshow('borders',rotatednewImage)
        cv2.imshow('superposition',rotatedCropImg)
        
        cv2.setMouseCallback('original_cropped',get_mouse)
        cv2.setMouseCallback('borders',get_mouse)
        while(1):
           k= cv2.waitKey(0)
           if k == ord('a'):
               print([mouseX,mouseY])
           else:
               break
    
    cv2.destroyAllWindows()
    plt.close("all")
    return (lb, rb)

class processed_imgs:
    value = 0



def main(n_fotos,photos_measurements,img_queue):
           
    while(processed_imgs.value<n_fotos):
        logger = mylog.log("convertImages","python_processing")
        im_path = img_queue.get(True)                           #wait 100 seconds to receive a new photo or crash
        current_angle = 360/n_fotos *processed_imgs.value
        processed_imgs.value = processed_imgs.value + 1             #it controls how many imgs are already processed 
        
        logger.info(im_path + "em processamento")
        cam_img = cv2.imread(im_path).astype('float')
    
        #figure out which y coordinate corresponds to 4th axis "y"
        #mandril_img = cam_img[crop_params.My_crop1:crop_params.My_crop2, crop_params.Mx_crop1:crop_params.Mx_crop2]
        #mandril_edges = findEdges(mandril_img, True,  threshholdblue=10, threshholdgreen=10, threshholdred=8, minsizesegment=220)
        #y_4th = 0.5*(mandril_edges[1][1] + mandril_edges[1][0])  
                
        #crop image where the workpiece is positioned
        crop_img = cam_img[crop_params.WPy_crop1:crop_params.WPy_crop2, crop_params.WPx_crop1:crop_params.WPx_crop2]
        #take 2 different portions of the workipiece image
        [lb, rb] = slice_and_findEdges(crop_img,num_slices=10,debug=True)
        
        y_4th = 516             #TODO: adjust this gambiarra BR
        lb2center = y_4th - lb
        rb2center = rb - y_4th
            
        logger.info(im_path + " processada")
        #Append results in photo_measurements. Obviously, replace processed_imgs.
        photos_measurements.append([lb2center,rb2center,np.deg2rad(current_angle)])
        img_queue.task_done() #finished processing with this image
        
    logger.info("Fim do pre-processamento de imagens")


