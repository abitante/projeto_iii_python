# -*- coding: utf-8 -*-
"""
This code and its dependences should be included in GREWEN
@author: willi
"""

import threading
import mylog
import dataAquisition
import convertImages
from queue import Queue

testing = True                          #set true for offline testing
n_points = 7                            #resolution to 4th axis mapping 
photos_measurements = []
img_queue = Queue()

logger = mylog.log("main","python_processing")  # Create a custom logger
thread_dataAquistion = threading.Thread(target = dataAquisition.main, args=[n_points,img_queue, testing])  #call data aquisition thread
thread_dataAquistion.start()
thread_imageProcessing = threading.Thread(target = convertImages.main, args=[n_points,photos_measurements,img_queue])  #call data aquisition thread
thread_imageProcessing.start()

thread_dataAquistion.join()
thread_imageProcessing.join()
#Process photos_measurements only after all photos_measurements were gathered (thread finished)
