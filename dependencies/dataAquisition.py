# -*- coding: utf-8 -*-
"""
This code is in charge of:

A02. Send command to CNC for it to move to initial position (distance sensor center perpendicular to W)
A03. Gather data
   C01. Take photo and distance measurements around the workpiece by commanding the fourth axis



@author: willi

"""

import mylog
import os
import shutil
import glob

filePath = 'C:\\Mach3\\ArquivosPlugin\\'
filePath_git= 'C:\\Users\\willi\\Documents\\ProjetoIII\\projeto_iii_python'
#filePath_git = 'C:\\Guilherme\\Repositorios\\Python'
filePath_testing_dataSet = 'C:\\Users\\willi\\Documents\\ProjetoIII\\projeto_iii_python\\ensaios_16\\Teste03\\'
camera_app = 'saveFrame.py'



def main(tcp, n_points,img_queue, testing):
    logger = mylog.log("dataAquisition","python_processing")
    #code begins here
    
    logger.info("Conectado ao socket")
 
    #TODO: read from text file
    #move above the 4th axis:
    logger.info("Posicionando acima do quarto eixo")
    msg = "G0 X204 Y115.88 Z0"               #<-----
    tcp.send(msg.encode())
    
    BUFFER_SIZE = 20          # Normally 1024, but we want fast response
    confirmation_msg = tcp.recv(BUFFER_SIZE)
    confirmation_msg = confirmation_msg.decode() #why you should use decode: https://stackoverflow.com/questions/21810776/str-encode-adds-a-b-to-the-front-of-data
    
    if(confirmation_msg != "Sucesso"):
        logger.info("Não foi bem sucedida a movimentação para o quarto eixo")
        
    ref = 0
    step = 360/(n_points)
    
    while (ref<360):
        #registra no log
        logger.info("Nova medida %.4f" %ref)
        #Send a G1 command to mach3
        msg = "G0 A%.4f" %ref                
        print(msg)
        tcp.send(msg.encode())
        confirmation_msg = tcp.recv(BUFFER_SIZE)  
        confirmation_msg = confirmation_msg.decode()
        if(confirmation_msg != "Sucesso"):
            logger.info("Não foi bem sucedida a movimentação para o quarto eixo")
          
                
        if(not testing):
        #TAKE A PICTURE
            os.chdir(filePath_git)
            os.system('python Scripts_ProjetoII//' + camera_app)                   
            #make a copy whose name is ref number
    
            std_name = filePath + 'image0.png'
            cpd_name = filePath + 'ref'+ ('%.4f'%ref) +'.png'
            cpd_name = cpd_name.replace(".","_",1)
            shutil.copy2(std_name,cpd_name)
            img_queue.put(cpd_name)
        else:       
            name = filePath_testing_dataSet + 'ref'+ ('%.4f'%ref) +'.png'
            name = name.replace(".","_",1)
            img_queue.put(name)
            
        ref = ref + step

    logger.info("Fim da execução do dataaquisition")
    
