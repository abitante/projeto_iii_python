#         (                           )  
# (       )\ )      (  (           ( /(  
# )\ )   (()/( (    )\))(   ' (    )\()) 
#(()/(    /(_)))\  ((_)()\ )  )\  ((_)\  
# /(_))_ (_)) ((_) _(())\_)()((_)  _((_) 
#(_)) __|| _ \| __|\ \((_)/ /| __|| \| | 
#  | (_ ||   /| _|  \ \/\/ / | _| | .` | 
#   \___||_|_\|___|  \_/\_/  |___||_|\_|    (shits on fire,yo)
'''
Python interface for 4 axis CNC auto zero.
It commands the CNC durring the process and collects data.
Data is processed and displayed to the user
User chooses the zero location on YZ plane and then face that is horizontal.
X zero position is auto-detected and is the end of the shape in the X dimension.
W dimension is the angle around -X

If one were to observe the CNC from the cameras poit of view:
          .
         /|\ X
          |
          |
    Y <---0 Z
The zero of the XYZ is the point of the mandrel when CNC is zeroed.

by Guilherme Raabe Abitante
'''
##########################################################
### if there are bugs, they are marked with BUG HERE ! ###
##########################################################
from tkinter import *
from tkinter import ttk
import tkinter
import numpy as np
import math
from functools import partial
import threading
import time
from re import sub


#for A02 and A03
import mylog
import dataAquisition
import convertImages
import socket
from queue import Queue
    
    
'''____                 _   _                 
 |  ___|   _ _ __   ___| |_(_) ___  _ __  ___ 
 | |_ | | | | '_ \ / __| __| |/ _ \| '_ \/ __|
 |  _|| |_| | | | | (__| |_| | (_) | | | \__ \
 |_|   \__,_|_| |_|\___|\__|_|\___/|_| |_|___/
'''
# Function is associated with "Advanced options" button
# It creates the Advanced window if it's non-existent. If it exists, smply bring it to the front
def spawnWindowAdvancedMenu():
    # BUG HERE ! this check-if-instatiated method must be validated.
    global referenceWindowAdvancedMenu
    global windowMain
    if referenceWindowAdvancedMenu != None:
        try:
            referenceWindowAdvancedMenu.top.deiconify()
        except:
            referenceWindowAdvancedMenu = classWindowAdvancedMenu(windowMain)
    else:
        referenceWindowAdvancedMenu = classWindowAdvancedMenu(windowMain)

def commandEditZero():
    global referenceEditZero
    global windowMain
    global selectedData
    global infoMessage
    if len(selectedData)>0:
        if referenceEditZero != None:
            try:
                referenceEditZero.top.deiconify()
            except:
                referenceEditZero = classEditZero(windowMain)
        else:
            referenceEditZero = classEditZero(windowMain)
        infoMessage.set("Edit Pop-up Focused.")
    else:
        infoMessage.set("Must Select a Point to Edit.")

class classEditZero:
    def __init__(self, root):
        self.top = tkinter.Toplevel(root)
        global windowShapeSize
        bbH = "1"
        bbW = "10"
        self.top.geometry('%dx%d+%d+%d' % (250, 150, 100 + windowShapeSize, 350))
        Label(self.top, text = "Edit Zero").pack()
        self.edittedY = Entry(self.top)
        self.edittedY.pack()
        self.edittedZ = Entry(self.top)
        self.edittedZ.pack()
        Button(self.top, text="OK", width=bbW, height=bbH, command=self.commandOK).pack()
        Button(self.top, text="Cancel", width=bbW, height=bbH, command=self.top.destroy).pack()
        self.message = StringVar()
        Label(self.top, textvariable = self.message).pack()
        self.message.set("--")

    def commandOK(self):
        try:
            Y = float(self.edittedY.get())
            Z = float(self.edittedZ.get())
            global rotatedFinalY
            global rotatedFinalZ
            rotatedFinalY.set("Y = %f" % (Y))
            rotatedFinalZ.set("Z = %f" % (Z))
            global selectedData
            selectedData[3] = Y
            selectedData[4] = Z
            self.message.set("Confirmed Conversion.")
            commandOK()
            self.top.destroy()
        except:
            self.message.set("Invalid input.")
        


def commandOK():
    global selectedData
    global infoMessage
    if len(selectedData)>0:
        infoMessage.set("Data ready and sending to CNC.")
        #global windowMain
        #windowMain.destroy()
    else:
        infoMessage.set("Zeroing data not ready.")
    
# Function is executed when an edge button is clicked.
# It updates the displayed data
def pointClick(i):
    global infoMessage
    infoMessage.set("Selected edge %d." % (i))
    global selectedY
    global selectedZ
    global finalEdgeCam
    selectedY.set("Unrotated Y = %.3f" % (finalEdgeCam[i][0]))
    selectedZ.set("Unrotated Z = %.3f" % (finalEdgeCam[i][1]))
    
    global alignCounterclockwiseFace
    if alignCounterclockwiseFace.get():
        if i == 0:
            next_i = len(finalEdgeCam) - 1
        else:
            next_i = i - 1
    else: # clockwise face is to be horizontal
        if i + 1 == len(finalEdgeCam):
            next_i = 0
        else:
            next_i = i + 1            
    angleOffset = math.atan2(finalEdgeCam[next_i][1]-finalEdgeCam[i][1],finalEdgeCam[next_i][0]-finalEdgeCam[i][0])
    baseAngle = math.atan2(finalEdgeCam[i][1],finalEdgeCam[i][0]) # Angle of vector (between origin and selected point) with Y axis
    baseModule = math.hypot(finalEdgeCam[i][1],finalEdgeCam[i][0])# Module of vector (between origin and selected point)
    global scale_snap
    rotatedY = round(baseModule*math.cos(baseAngle + angleOffset), -scale_snap.get())
    rotatedZ = round(baseModule*math.sin(baseAngle + angleOffset), -scale_snap.get())
    
    global rotatedFinalY
    global rotatedFinalZ
    global rotatedFinalW
    rotatedFinalY.set("Y = %f" % (rotatedY))
    rotatedFinalZ.set("Z = %f" % (rotatedZ))
    rotatedFinalW.set("W = %.3f" % (angleOffset))
    global selectedData
    selectedData = [i, finalEdgeCam[i][0], finalEdgeCam[i][1], rotatedY, rotatedZ, angleOffset]

'''____ _                         
  / ___| | __ _ ___ ___  ___  ___ 
 | |   | |/ _` / __/ __|/ _ \/ __|
 | |___| | (_| \__ \__ \  __/\__ \
  \____|_|\__,_|___/___/\___||___/
'''
class classWindowShape:
    # Class responsible for displaying aquired shape and entertaining the user
    def __init__(self, root, w, x, y):
        self.top = tkinter.Toplevel(root)
        self.top.geometry("%dx%d+%d+%d" % (w, w, x, y))
        
        self.canvas = tkinter.Canvas(self.top, width=w, height=w, background='white')
        self.canvas.pack(fill="both", expand=1)

        self.scale = w # How many mm fit in a pixel (must be updated later)
        self.width = int(w) # Size in pixels of canvas
        self.finalEdgeButtons = [] # List to store button IDs

    # Updates the image displayed while data is beeing gathered
    def animate(self, running):
        im1 = [PhotoImage(file="frames/frame_0 (%d).gif" % (i)) for i in range(1,19)]
        frameID = self.canvas.create_image(self.width/2, self.width/2, image = im1[0])
        frameCount = 0
        while running.is_set():
            frameCount = frameCount + 1
            if frameCount > 17:
                frameCount = 0
            self.canvas.itemconfig(frameID, image = im1[frameCount])
            time.sleep(0.1)
        self.canvas.delete("all")#frameID)
        print("ANIMATION Terminated.")

    # Draws coordinate system, scale and center os drawing
    def prepareCanvas(self, distantPoint):
        # Drawing scale in window
        self.scale = 0.4*self.width/distantPoint
        print("+ Scale [pixel/mm] set as: %f" % self.scale)
        self.canvas.create_line(self.width-20,self.width-20,self.width-20,self.width-30)
        self.canvas.create_line(self.width-20,self.width-20,self.width-20-self.scale,self.width-20)
        self.canvas.create_line(self.width-20-self.scale,self.width-20,self.width-20-self.scale,self.width-30)
        ttk.Label(self.top,text="1 mm").place(x = self.width-55,y = self.width-19)
        # Drawing Coord System
        lineLenght = 30
        coorA = [self.width-20,self.width-50]
        coorB = [coorA[0]-lineLenght,coorA[1]]
        coorC = [coorB[0],coorB[1]-lineLenght]
        arrowLenght= 8
        arrowWidth = 4
        self.canvas.create_line(coorA[0],coorA[1],coorA[0]-arrowLenght,coorA[1]-arrowWidth)
        self.canvas.create_line(coorA[0],coorA[1],coorB[0],coorB[1])
        self.canvas.create_line(coorA[0],coorA[1],coorA[0]-arrowLenght,coorA[1]+arrowWidth)
        self.canvas.create_line(coorC[0],coorC[1],coorC[0]-arrowWidth, coorC[1]+arrowLenght)
        self.canvas.create_line(coorC[0],coorC[1],coorB[0],coorB[1])
        self.canvas.create_line(coorC[0],coorC[1],coorC[0]+arrowWidth, coorC[1]+arrowLenght)
        ttk.Label(self.top,text="Z").place(x = coorC[0]+arrowWidth+2,y = coorC[1])
        ttk.Label(self.top,text="Y").place(x = coorA[0],y = coorA[1]-arrowWidth-12)
        # Drawing center (+)
        self.canvas.create_line(self.width*0.5, self.width*0.46, self.width*0.5, self.width*0.54, fill="red")
        self.canvas.create_line(self.width*0.46, self.width*0.5, self.width*0.54, self.width*0.5, fill="red")

    # Draws detected cylinder and displays data.
    def populateCylinder(slef, radius, dataCamDeviation):
        print('populateCylinder in classWindowShape...')
        self.prepareCanvas(radius)
        xy0 = (self.width-ptSize)/2 - radius
        xy1 = (self.width-ptSize)/2 + radius
        self.canvas.create_oval(xy0,xy0,xy1,xy1)
        global selectedY
        global selectedZ
        global rotatedFinalY
        global rotatedFinalZ
        global rotatedFinalW
        selectedY.set("(There are no edges)")
        selectedZ.set(" ")
        rotatedFinalW.set("Shape is a cylinder,") 
        rotatedFinalY.set("with %.3f mm radius" % (radius))
        rotatedFinalZ.set("and %.3f deviation." % (dataCamDeviation))

    # Draws shape in canvas and places edge buttons
    def populateResults(self, pts, ptSize, butImage):
        distantPoint = 0
        print('populateResults in classWindowShape...')
        # Searching for most distant point
        for i in range(0,len(pts)):                 # Se em um ponto do conjunto
            for j in range(0,2):                    # uma das duas coordenadas deste ponto
                if abs(pts[i][j]) > distantPoint:   # for a maior em todo o conjunto
                    distantPoint = abs(pts[i][j])   # esse ponto é o absoluto mais distante
        print("+ Furthest Point: %d" % distantPoint)  # (pq a tela eh quadrada entao foda-se o modulo)

        self.prepareCanvas(distantPoint)

        print('+ Positioning Buttons')
        if self.finalEdgeButtons != None:
            print("++ Probable Memory Leakege!!")
        self.finalEdgeButtons = [] # to be sure...
        butPos = []
        for i in range(0,len(pts)):
            # points Z axis is inverted due to window "y" axis logic
            butPosX = int( pts[i][0]*self.scale + (self.width-ptSize)/2)
            butPosY = int(-pts[i][1]*self.scale + (self.width-ptSize)/2)
            butPos.append([butPosX,butPosY])
            self.finalEdgeButtons.append(tkinter.Button(self.top, command=partial(pointClick,i), image=butImage,width="%d"%ptSize,height="%d"%ptSize, borderwidth=0, background="#FFFFFF"))
            self.finalEdgeButtons[i].place(x = butPosX,y = butPosY)
        
        print("+ Drawing Lines and Labels")
        for i in range(0,len(butPos)):
            if i + 1 == len(butPos):
                next_i = 0
            else:
                next_i = i + 1
            self.canvas.create_line(butPos[i][0]+ptSize/2, butPos[i][1]+ptSize/2, butPos[next_i][0]+ptSize/2, butPos[next_i][1]+ptSize/2)
            lineLenght = abs(math.hypot(pts[i][0]-pts[next_i][0],pts[i][1]-pts[next_i][1]))
            ttk.Label(self.top,text= ("%.3f"%lineLenght)).place(x = (butPos[i][0]+butPos[next_i][0])/2,y = (butPos[i][1]+butPos[next_i][1])/2)
            
# Class responsible for debug plots    
class classWindowAdvancedMenu:
    def __init__(self, root):
        global windowShapeSize
        self.root = root
        self.menu = tkinter.Toplevel(root)
        self.menu.geometry("%dx%d+%d+%d" % (250, 500, 400 + windowShapeSize, 50))
        bbH = "1"
        bbW = "30"
        Label(self.menu, text =  "- Camera Algorithm -").pack()
        Button(self.menu, text="Plot Camera Data", width=bbW, height=bbH, command=self.plotCameraData).pack()
        Label(self.menu, text =  " ").pack()

        self.top = tkinter.Toplevel(root)
        self.top.geometry("%dx%d+%d+%d" % (windowShapeSize, windowShapeSize, 50, 50))
        self.canvas = tkinter.Canvas(self.top, width=windowShapeSize, height=windowShapeSize, background='white')
        self.canvas.pack(fill="both", expand=1)

        self.labelList = []
        self.scale = windowShapeSize # How many mm fit in a pixel (must be updated later)
        self.width = int(windowShapeSize) # Size in pixels of canvas
        self.canvas.create_line(self.width*0.5, self.width*0.1, self.width*0.5, self.width*0.9, fill="red")
        self.canvas.create_line(self.width*0.1, self.width*0.5, self.width*0.9, self.width*0.5, fill="red")

    def grphLine(self,a,b,c,d,color):
        self.canvas.create_line(self.width*0.5+a*self.scale, self.width*0.5-b*self.scale, self.width*0.5+c*self.scale, self.width*0.5-d*self.scale, fill=color)
        
    def grphThickLine(self,a,b,c,d,color,s):
        self.canvas.create_line(self.width*0.5+a*self.scale, self.width*0.5-b*self.scale, self.width*0.5+c*self.scale, self.width*0.5-d*self.scale, fill=color,width = s)
        
    def plotCameraData(self):
        #BUG HERE! The window is not recreated if user closes it
        self.canvas.delete("all")
        if (len(self.labelList)>0) :
            for label in self.labelList: label.destroy()
        self.labelList = []
        #global finalEdgeCam #[[y in mm, z in mm, number of edge candidates used, error ], other measures .... ]
        global radiusCam
        global Ycam # Y coord of camera for each measure
        global Zcam # Z coord of camera for each measure
        global Ybase # Y coord of base for each measure
        global Zbase # Z coord of base for each measure
        global Yproj # Y coord of projection for each measure
        global Zproj # Z coord of projection for each measure
        global dataCam
        labelDistance = 10
        self.scale = (windowShapeSize/2 - 30)/(math.sqrt(Ycam[0]*Ycam[0]+Zcam[0]*Zcam[0]))
        print("Advanced - Plotting Camera Data with scale %.3f" % (self.scale))
        if radiusCam == None:
            for i in range(0,len(dataCam)):
                self.grphLine(Ycam[i],Zcam[i],Ybase[i],Zbase[i],"grey") # lines from camera to base
                self.labelList.append(ttk.Label(self.top,text=("%.3f"%dataCam[i][2])))
                self.labelList[i].place(x = self.width*0.5+Ycam[i]*self.scale+labelDistance,y = self.width*0.5-Zcam[i]*self.scale+labelDistance)
                self.grphLine(Ycam[i],Zcam[i],Yproj[i],Zproj[i],"blue") # lines from camera to projection
                self.grphLine(Ybase[i],Zbase[i],Yproj[i],Zproj[i],"cyan") # lines from base to projection

'''
     _    _                  _ _   _               
    / \  | | __ _  ___  _ __(_) |_| |__  _ __ ___  
   / _ \ | |/ _` |/ _ \| '__| | __| '_ \| '_ ` _ \ 
  / ___ \| | (_| | (_) | |  | | |_| | | | | | | | |
 /_/   \_\_|\__, |\___/|_|  |_|\__|_| |_|_| |_| |_|
            |___/
'''
# A01. Setup
#   B01. Create window with "gathering data gif", where user can already choose the zeroing parameters
#   B02. Create thread that executes the gif update
#   B03. Open .txt and load known info
#       - XYZ of initial position (distance sensor center perpendicular to W)
#       - XYZ Sensor offset
#       - XYZ Camera offset
#       - XYZ of Center of 4th axis
#       - Length in pixels of a 1m line captured by the camera when at initial position (to calculate d)
#       - Angle between sensor measures
#       - Step of camera measures (how many sensor measures are taken between camera measures)

# A02. Send command to CNC for it to move to initial position (distance sensor center perpendicular to W)

# A03. Gather data
#   C01. Take photo measurements around the workpiece by commanding W
#       D01. Convert each image into two projection measurements
'''
Information transfered:
    list of camera measurements : dataCam = [[distance in pixels from center to left,
                                                distance in pixels from center to right,
                                                    angle of photo], other measures .... ]
'''
# A04. Thread CNC command
#   E01. Command CNC to hover over the apparatus
# A05. Form shape edge list using projection measurements 
'''
    list of camera generated edges : finalEdgeCam = [[y of edge in mm, z of edge in mm,
                                                        number of edge candidates used,
                                                            error of the candidates with the result in mm],
                                                                other measures .... ]
'''
# A08. Display edges and faces of the aquired shape, where edges are clickable and set the zero to themselves
# A09. Calculate errors 
# A10. Wait for user input, which can be: 
#   - Change in parameter           (return to A07)
#   - Checkbox to determine which of the faces that meet on the edge is to be set horizontally
#   - Open advanced window          (Open popup)
#       - Advanced options:
#           - Show devils work on second window
#   - Click on edge
#   - Accept zero position
#   - Cancel operation
#   - Edit zero position
#       - Accept edited zero position
#       - Cancel edit

'''  _       ___    _ 
    / \     / _ \  / |
   / _ \   | | | | | |
  / ___ \  | |_| | | |
 /_/   \_\  \___/  |_|
'''
print("Started A01")
# Create Main Window
windowMain = Tk()
windowMain.title("GREWEN")
# Determining Shape Window size
ws = windowMain.winfo_screenwidth()  # Width  of the screen
hs = windowMain.winfo_screenheight() # Height of the screen
if ws > hs :
    ws = hs
else :
    hs = ws
windowShapeSize = 0.8*ws # Secondary window size is 80% of the smaller screen size
print("Calculated Shape Window Size: %d" % windowShapeSize)
# Positioning of Both Windows
windowShape = classWindowShape(windowMain, windowShapeSize, 50, 50)
windowMain.geometry('%dx%d+%d+%d' % (250, 500, 100 + windowShapeSize, 50))

#### BEAUTIFUL "GLOBALS" ####
overallErr = StringVar()
selectedY = StringVar()
selectedZ = StringVar()
rotatedFinalY = StringVar()
rotatedFinalZ = StringVar()
rotatedFinalW = StringVar()
alignCounterclockwiseFace = IntVar()
infoMessage = StringVar()

radiusCam = None
Ycam = None # Y coord of camera for each measure
Zcam = None # Z coord of camera for each measure
Ybase = None # Y coord of base for each measure
Zbase = None # Z coord of base for each measure
Yproj = None # Y coord of projection for each measure
Zproj = None # Z coord of projection for each measure
dataCam = None

finalEdgeCam = [] # Final result of algorithm
selectedData = [] # Data selected by user [i, Y of edge, Z of edge, rotatedY, rotatedZ, angleOffset]

# Main Window Widget Creation
ttk.Label(windowMain,text="Positioning Resolution (mm)").pack()
scale_snap = Scale(windowMain, from_=-6, to=1, resolution=1, orient=HORIZONTAL, length=200)
scale_snap.set(-3)
scale_snap.pack()

overallErr.set("not initialized")
selectedY.set("Unrotated Y = %.3f" % (0))
selectedZ.set("Unrotated Z = %.3f" % (0))
rotatedFinalY.set("Y = %.3f" % (0))
rotatedFinalZ.set("Z = %.3f" % (0))
rotatedFinalW.set("W = %.3f" % (0))
infoMessage.set(" ")

Label(windowMain, text = "------------------------").pack()
Checkbutton(windowMain, text="Counterclockwise Reference Plane",variable=alignCounterclockwiseFace, command=alignCounterclockwiseFace).pack()
Label(windowMain, text =  "Selected Edge").pack()
Label(windowMain, textvariable = selectedY).pack()
Label(windowMain, textvariable = selectedZ).pack()
Label(windowMain, text = "------------------------").pack()
Label(windowMain, text =  "Coordinate System Offset").pack()
Label(windowMain, textvariable = rotatedFinalY).pack()
Label(windowMain, textvariable = rotatedFinalZ).pack()
Label(windowMain, textvariable = rotatedFinalW).pack()
Label(windowMain, text = "------------------------").pack()
Label(windowMain, text =  "Overall Error").pack()
Label(windowMain, textvariable = overallErr).pack()
Label(windowMain, text = "------------------------").pack()

bbH = "1"
bbW = "10"
referenceWindowAdvancedMenu = None
referenceEditZero = None
Button(windowMain, text="Advanced", width=bbW, height=bbH, command=spawnWindowAdvancedMenu).pack()
Label(windowMain, text = "------------------------").pack()
Button(windowMain, text="OK", width=bbW, height=bbH, command=commandOK).pack()
Button(windowMain, text="Edit Zero", width=bbW, height=bbH, command= commandEditZero).pack()
Button(windowMain, text="Cancel", width=bbW, height=bbH, command= windowMain.destroy).pack()
Label(windowMain, textvariable = infoMessage).pack()

#   B02. Create thread that executes the gif update
threadAnimateRunning = threading.Event()
threadAnimateRunning.set()
threadAnimate = threading.Thread(target=windowShape.animate, args=(threadAnimateRunning,))
threadAnimate.start()

#   B03. Open .txt and load known info
'''
cteXYZinitialPos = [0, 0, 0]
cteXYZoffsetSensor = [None, None, None]
cteXYZoffsetCamera = [0, 0, 254] 
cteXYZfourthAxisPos = [0, 0, 0] 
cteZtableToAxis = 40 # hp = 40
d = 50/279 
cteNumSensorMeasures = 72
cteNumPictures = 24
'''
f = open("GREWENconfiguration.txt","r")
configuration = f.readlines()

lineSplit = configuration[0].split(',')
cteXYZinitialPos = [float(lineSplit[1]),float(lineSplit[2]),float(lineSplit[3])]
lineSplit = configuration[1].split(',')
cteXYZoffsetSensor = [float(lineSplit[1]),float(lineSplit[2]),float(lineSplit[3])]
lineSplit = configuration[2].split(',')
cteXYZoffsetCamera = [float(lineSplit[1]),float(lineSplit[2]),float(lineSplit[3])]
lineSplit = configuration[3].split(',')
cteXYZfourthAxisPos = [float(lineSplit[1]),float(lineSplit[2]),float(lineSplit[3])]
lineSplit = configuration[4].split(',')
cteZtableToAxis = float(lineSplit[1])
lineSplit = configuration[5].split(',')
d = float(lineSplit[1])/float(lineSplit[2])  # mm/pix
lineSplit = configuration[6].split(',')
cteNumSensorMeasures = float(lineSplit[1])
lineSplit = configuration[7].split(',')
cteNumPictures = float(lineSplit[1])

print("- XYZ of initial position (distance sensor center perpendicular to W) : %.f %.f %.f" %(cteXYZinitialPos[0],cteXYZinitialPos[1],cteXYZinitialPos[2]))
print("- XYZ Sensor offset : %.f %.f %.f" %(cteXYZoffsetSensor[0],cteXYZoffsetSensor[1],cteXYZoffsetSensor[2]))
print("- XYZ Camera offset   # hc = 254 = cteXYZoffsetCamera[2] + cteXYZinitialPos[0] : %.f %.f %.f" %(cteXYZoffsetCamera[0],cteXYZoffsetCamera[1],cteXYZoffsetCamera[2]))
print("- XYZ of Center of 4th axis : %.f %.f %.f" %(cteXYZfourthAxisPos[0],cteXYZfourthAxisPos[1],cteXYZfourthAxisPos[2]))
print("- Z distance from table to 4th axis : %.f" %(cteZtableToAxis))
print("- Length in pixels of a 1m line captured by the camera when at initial position (to calculate d) : %.f" %(d))
print("- Number of sensor measures (must be multiple of cteNumPictures) : %.f" %(cteNumSensorMeasures))
print("- Number of camera pictures : %.f" %(cteNumPictures))


#stores tcp socket data
'''
Setup client socket
'''
#before starting the interface, connect to VS2008
HOST = '127.0.0.1'      #local host
TCP_PORT = 9999

tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
dest = (HOST, TCP_PORT)
tcp.connect(dest)

# mainProcessing is the thread that executes our main algorithm
def mainProcessing():    
    global dataCam
    global infoMessage
    '''  _       ___    ____  
        / \     / _ \  |___ \ 
       / _ \   | | | |   __) |
      / ___ \  | |_| |  / __/ 
     /_/   \_\  \___/  |_____|
    '''
    print("Started A02") # Send command to CNC for it to move to initial position (distance sensor center perpendicular to W)
    infoMessage.set("Commanding CNC to Initial Position.")
      
    testing = True                          #set true for offline testing
    if(testing):
        n_points = input("Insira o número de fotos do ensaio de teste: ")
        n_points = int(n_points)
    else:
        n_points = 36                           #resolution to 4th axis mapping . TODO: it's probably wrong. check the adequate variable
                                            #if it's in testing mode
                                            
    photos_measurements = []
    img_queue = Queue()
    
    logger = mylog.log("main","python_processing")  # Create a custom logger
    thread_dataAquistion = threading.Thread(target = dataAquisition.main, args=[tcp, n_points,img_queue, testing])  #call data aquisition thread
    thread_dataAquistion.start()
    thread_imageProcessing = threading.Thread(target = convertImages.main, args=[n_points,photos_measurements,img_queue])  #call data aquisition thread
    thread_imageProcessing.start()
    
    thread_dataAquistion.join()
    thread_imageProcessing.join()
    #Process photos_measurements only after all photos_measurements were gathered (thread finished)
    
    '''  _       ___    _____ 
        / \     / _ \  |___ / 
       / _ \   | | | |   |_ \ 
      / ___ \  | |_| |  ___) |
     /_/   \_\  \___/  |____/
    '''
    print("Started A03")# Gather data
    infoMessage.set("Gathering Data")
    #   C01. Take photo and distance measurements around the workpiece by commanding W
    #       D01. Convert each image into two projection measurements $$ Ernesto $$
    '''
    list of sensor measurements = dataSensor = [[y position in mm of data point,
                                                    z position in mm of data point],
                                                        other measures... ]
    list of camera measurements : dataCam = [[distance in pixels from center to left,
                                                distance in pixels from center to right,
                                                    angle of photo], other measures .... ]
    '''    
    # while sockets arent ready, points are entered manually
    dataSensor = [[10,10],[5,10],[0,10],[-5,10],
              [-10,10],[-10,5],[-10,0],[-10,-5],
              [-10,-10],[-5,-10],[0,-10],[5,-10],
              [10,-10],[10,-5],[10,0],[10,5]]#[[y,z],...]
#    dataCam = [[198,0,math.radians(15)],
#            [225,0,math.radians(30)],
#            [239,0,math.radians(45)],
#            [238,0,math.radians(60)],
#            [219,0,math.radians(75)],
#            [225,0,math.radians(90)],
#            [236,0,math.radians(105)],
#            [234,0,math.radians(120)],
#            [215,0,math.radians(135)],
#            [176,0,math.radians(150)],
#            [121,0,math.radians(165)],
#            [159,0,math.radians(180)],
#            [201,0,math.radians(195)],
#            [234,0,math.radians(210)],
#            [253,0,math.radians(225)],
#            [255,0,math.radians(240)],
#            [238,0,math.radians(255)],
#            [245,0,math.radians(270)],
#            [259,0,math.radians(285)],
#            [251,0,math.radians(300)],
#            [225,0,math.radians(315)],
#            [180,0,math.radians(330)],
#            [119,0,math.radians(345)],
#            [160,0,math.radians(360)]]
#    
    dataCam = photos_measurements
    
    dataFile = open("measurements.txt","w+")
    dataFile.write("List generated from sensor data:\n\n")
    for i in range(0, len(dataSensor)):
        dataFile.write(str(dataSensor[i])+"\n")
    dataFile.write("\n\nList generated from camera data:\n\n")
    for i in range(0, len(dataCam)):
        dataFile.write(str(dataCam[i])+"\n")
    dataFile.close()
    
    '''  _       ___    _  _   
        / \     / _ \  | || |  
       / _ \   | | | | | || |_ 
      / ___ \  | |_| | |__   _|
     /_/   \_\  \___/     |_|
    '''
    print("Started A04")# Thread CNC command
    infoMessage.set("Positioning CNC over apparatus.")
    #   E01. Command CNC to hover over the apparatus

    '''  _       ___    ____  
        / \     / _ \  | ___| 
       / _ \   | | | | |___ \ 
      / ___ \  | |_| |  ___) |
     /_/   \_\  \___/  |____/
    '''
    print("Started A05")
    infoMessage.set("Calculating shape.")
    # A05. Form shape edge list using projection measurements
    
    cteZaxisToCam = cteXYZinitialPos[2] + cteXYZoffsetCamera[2] - cteXYZfourthAxisPos[2]
    cteYaxisToCam = cteXYZinitialPos[1] + cteXYZoffsetCamera[1] - cteXYZfourthAxisPos[1]
    hcp = math.sqrt(cteZaxisToCam*cteZaxisToCam + cteYaxisToCam*cteYaxisToCam) # distance of camera from 4th axis
    hp = cteZtableToAxis # Perpendicular distance from plane where "d" comes from to the fourth axis
    cteWhcpToVerticalAroundAxis = math.atan2(cteYaxisToCam, cteZaxisToCam)

    # Checking if its a cylinder:
    dataCamDeviation = 0
    dataCamMean = 0
    for i in range(0, len(dataCam)):
        dataCamMean = dataCamMean + dataCam[i][0] + dataCam[i][1]
    dataCamMean = dataCamMean/(2*len(dataCam))
    for i in range(0, len(dataCam)):
        dataCamDeviation = dataCamDeviation + (dataCam[i][0] - dataCamMean)*(dataCam[i][0] - dataCamMean) + (dataCam[i][1] - dataCamMean)*(dataCam[i][1] - dataCamMean)
    dataCamDeviation = math.sqrt(dataCamDeviation/(2*len(dataCam)))

    global radiusCam
    radiusCam = None
    if (dataCamDeviation/dataCamMean < 0.04):   # If the measures deviate less than 4% radius
        radiusCam = dataCamMean*d*hcp/(hcp + hp)# it is a cylinder.
        print("Cylinder was detected, with %.3f mm radius." %(radiusCam))
    else:
        #   Z /|\   .
        #      |   /|\   
        # Y <--.   /
        #       __/  Angles (measured from Z)
        global Ycam
        global Zcam
        global Ybase
        global Zbase
        global Yproj
        global Zproj
        Ycam = []  # Y coord of camera for each measure
        Zcam = []  # Z coord of camera for each measure
        Ybase = [] # Y coord of base for each measure
        Zbase = [] # Z coord of base for each measure
        Yproj = [] # Y coord of projection for each measure
        Zproj = [] # Z coord of projection for each measure
        for i in range(0,len(dataCam)):
            Ycam.append(hcp*math.sin(dataCam[i][2] + cteWhcpToVerticalAroundAxis))
            Zcam.append(hcp*math.cos(dataCam[i][2] + cteWhcpToVerticalAroundAxis))
            Ybase.append(hp*math.sin(dataCam[i][2] + cteWhcpToVerticalAroundAxis + math.pi))
            Zbase.append(hp*math.cos(dataCam[i][2] + cteWhcpToVerticalAroundAxis + math.pi))
            Yproj.append(Ybase[i] + d*dataCam[i][0]*math.sin(dataCam[i][2] + cteWhcpToVerticalAroundAxis - math.pi/2))
            Zproj.append(Zbase[i] + d*dataCam[i][0]*math.cos(dataCam[i][2] + cteWhcpToVerticalAroundAxis - math.pi/2))

        # Generate list of coefficients of lines between camera and projection:
        # Using (z = A.y + B) or (y = A.z + B) to express lines
        coef = [] # [[A,B], flag ], if (flag == 1){y = A.z + B}else{z = A.y + B}
        for i in range(0,len(dataCam)):
            if abs(Ycam[i]-Yproj[i]) < abs(Zcam[i]-Zproj[i]) : # Using y = A.z + B equasion
                A = (Ycam[i]-Yproj[i])/(Zcam[i]-Zproj[i])
                coef.append([[A,Ycam[i]-A*Zcam[i]],1])
                print("+ Measure regression %d resulted in z = %.3f.y + %.3f" % (i,coef[i][0][0],coef[i][0][1]))
            else: # Using z = A.y + B equasion
                A = (Zcam[i]-Zproj[i])/(Ycam[i]-Yproj[i])
                coef.append([[A,Zcam[i]-A*Ycam[i]],0])
                print("+ Measure regression %d resulted in y = %.3f.z + %.3f" % (i,coef[i][0][0],coef[i][0][1]))

        # Generate list of intersections of "coef" lines:
        intersection = []# [[y,z],...]
        for i in range(0,len(dataCam)):
            if i  == len(dataCam)-1:
                next_i = 0
            else:
                next_i = i + 1
                
            if coef[i][1] == 1: # y1 = A.z1 + B
                if coef[next_i][1] == 1: # y2 = A.z2 + B
                    # y = A1.z + B1 = A2.z + B2 -> z = (B1-B2)/(A2-A1)
                    z = (coef[i][0][1]-coef[next_i][0][1])/(coef[next_i][0][0]-coef[i][0][0])
                    intersection.append([coef[i][0][0]*z+coef[i][0][1],z])
                else: # z2 = A.y2 + B
                    # y = A1.z + B1 = (z - B2)/A2
                    if coef[next_i][0][0] == 0:# if A2 = 0
                        z = coef[next_i][0][1] #   z = B2
                    else:                   # else: B1.A2 + B2 = (1 - A1.A2).z
                        z = (coef[i][0][1]*coef[next_i][0][0] + coef[next_i][0][1])/(1 - coef[i][0][0]*coef[next_i][0][0])
                    intersection.append([coef[i][0][0]*z+coef[i][0][1],z])
            else: # z1 = A.y1 + B
                if coef[next_i][1] == 1: # y2 = A.z2 + B
                    # y = A2.z + B2 = (z - B1)/A1
                    if coef[i][0][0] == 0:  # if A1 = 0
                        z = coef[i][0][1]   #   z = B1
                    else:                   # else: B2.A1 + B1 = (1 - A1.A2).z
                        z = (coef[next_i][0][1]*coef[i][0][0] + coef[i][0][1])/(1 - coef[i][0][0]*coef[next_i][0][0])
                    intersection.append([coef[next_i][0][0]*z+coef[next_i][0][1],z])
                else: # z2 = A.y2 + B
                    # z = A1.y + B1 = A2.y + B2 -> y = (B1-B2)/(A2-A1)
                    z = (coef[i][0][1]-coef[next_i][0][1])/(coef[next_i][0][0]-coef[i][0][0]) # reusing var (read z as y)
                    intersection.append([z,coef[i][0][0]*z+coef[i][0][1]])

        distance = [] # distance between subsequent intersections
        edgeData = [] # # List of unprocessed possible edges (list of lists that make up an edge)
        edgeCounter = 0 # Position in edge list of the current edge
        maxDistance = 5 # distance in mm that triggers edge change, maybe change to .txt?
        for i in range(0, len(intersection)):
            # Add intersection to current list
            if len(edgeData) <= edgeCounter:        # If there are no elements in current edgeData[],
                edgeData.append([intersection[i]])  # create the list.
            else:
                edgeData[edgeCounter].append(intersection[i])
            # Check distace to next intersection
            if i  == len(intersection)-1:
                next_i = 0
            else:
                next_i = i + 1
            d1 = intersection[i][0] - intersection[next_i][0]
            d2 = intersection[i][1] - intersection[next_i][1]
            distance.append(math.sqrt(d1*d1+d2*d2))
            # If distance to next is great, next is a new edge
            if distance[i] > maxDistance:
                edgeCounter = edgeCounter + 1
            print("+ Regression %d intersects the next at Y = %.3f Z = %.3f"%(i,intersection[i][0],intersection[i][1]))
            print("++ Instersection %d is part of edge %d and is %.3fmm appart from the next."%(i,len(edgeData)-1, distance[i]))
      
        if distance[len(distance)-1] < maxDistance :
            # The last and first element of edgeData are the same edge and should join.
            edgeData[0].extend(edgeData[len(edgeData)-1])
            edgeData.pop(len(edgeData)-1)

        edgeCam = [] # List of processed possible edges: [[y in mm, z in mm, number of intersections used to aproximate edge, error in mm], ... ]
        y = 0
        z = 0
        error = 0
        for i in range(0, len(edgeData)):
            # Aproximating real edge as the mean of data (dumb way)
            # it would be better to search for minimum quadratic distance, but data is already bad (not worth it?)
            for j in range(0, len(edgeData[i])):
                y = y + edgeData[i][j][0]
                z = z + edgeData[i][j][1]
            y = y/len(edgeData[i])
            z = z/len(edgeData[i])
            for j in range(0, len(edgeData[i])):
                error = error + math.sqrt((edgeData[i][j][0]-y)*(edgeData[i][j][0]-y)+(edgeData[i][j][1]-z)*(edgeData[i][j][1]-z))
            edgeCam.append([y,z,len(edgeData[i]),error])
            print("+ Possible Edge %d is at Y= %.3f Z=%.3f and was calculated using %d points with %.3f error."%(i,edgeCam[i][0],edgeCam[i][1],edgeCam[i][2],edgeCam[i][3]))
            y = 0
            z = 0
            error = 0

        # Single data calculated edges are just ilusions.... result must be cleansed
        # Ilusions occur when subsequent pictures capture different edges or there has been a significative mesurement error
        global finalEdgeCam
        finalEdgeCam = []
        for i in range(0, len(edgeCam)):
            if edgeCam[i][2] > 1:
                finalEdgeCam.append(edgeCam[i])
        for i in range(0, len(finalEdgeCam)):
            print("+ Final Camera-detected Edge at Y= %.3f Z= %.3f" % (finalEdgeCam[i][0],finalEdgeCam[i][1]))

    '''
    list of camera generated edges : finalEdgeCam = [[y of edge in mm, z of edge in mm,
                                                        number of edge candidates used,
                                                            error of the candidates with the result in mm],
                                                                other measures .... ]
    '''

    dataFile = open("finalEdgeCam.txt","w+")
    dataFile.write("finalEdgeCam = [[y mm, z mm, number of edge candidates used, error of the candidates with the result in mm], .... ]\n\n")
    for i in range(0, len(finalEdgeCam)):
        dataFile.write(str(finalEdgeCam[i])+"\n")
    dataFile.close()

    '''  _       ___     ___  
        / \     / _ \   ( _ ) 
       / _ \   | | | |  / _ \ 
      / ___ \  | |_| | | (_) |
     /_/   \_\  \___/   \___/
    '''
    print("Started A08")
    # A08. Display edges and faces of the aquired shape, where edges are clickable and set the zero to themselves
    # Stop the animated fun:
    global threadAnimateRunning
    global threadAnimate
    threadAnimateRunning.clear()
    threadAnimate.join()
    # Draw results in windowShape:
    pointImageNormal = PhotoImage(file="bpt.gif")
    pointImageExcluded = PhotoImage(file="rpt.gif")
    pointImageForced = PhotoImage(file="pt.gif")
    global windowShape
    if radiusCam == None:
        windowShape.populateResults(finalEdgeCam, pointImageNormal.width(), pointImageNormal)
    else:
        windowShape.populateCylinder(radiusCam, dataCamDeviation)

    '''  _       ___     ___  
        / \     / _ \   / _ \ 
       / _ \   | | | | | (_) |
      / ___ \  | |_| |  \__, |
     /_/   \_\  \___/     /_/
    '''
    print("Started A09")
    # A09. Calculate errors
    global overallErr

    errorOverall = 0
    for i in range(0, len(finalEdgeCam)):
        errorOverall = errorOverall + finalEdgeCam[i][3]/finalEdgeCam[i][2]

    errorOverall = errorOverall/len(finalEdgeCam)
    overallErr.set("%.3f" % (errorOverall))

    
    infoMessage.set("Main Processing Over.")
    while True:
        continue

threading.Thread(target = mainProcessing).start()
mainloop() # for the interface, later there must be dedicated thread


#after everything is finished
print("Finalizando o socket")
msg = '#quit'
tcp.send(msg.encode())
#end communication
tcp.close() #close socket
